﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         24-01-2020
 *  
 *  TITLE:        Cip Exception object
 *  DESCRIPTION:  Object used to manage Cip Exceptions
 */

using System;
using System.Collections.Generic;

namespace Cip
{
  /// <summary>
  /// Object used to manage Cip Exceptions
  /// </summary>
  public class CipException : Exception
  {
    static Dictionary<CipStatus, string> Messages = new Dictionary<CipStatus, string>()
    {
      { CipStatus.SUCCESS, "Success" },
      { CipStatus.CONNECTION_FAILURE, "Connection failure" },
      { CipStatus.RESOURCE_UNAVAILABLE, "Resource unavailable" },
      { CipStatus.INVALID_PARAMETER_VALUE, "Invalid parameter value" },
      { CipStatus.PATH_SEGMENT_ERROR, "Path segment error" },
      { CipStatus.PATH_DESTINATION_UNKNOWN, "Path destination unknown" },
      { CipStatus.PARTIAL_TRANSFER, "Partial transfer" },
      { CipStatus.CONNECTION_LOST, "Connection lost" },
      { CipStatus.SERVICE_NOT_SUPPORTED, "Service not supported" },
      { CipStatus.INVALID_ATTRIBUTE_VALUE, "Invalid attribute value" },
      { CipStatus.ATTRIBUTE_LIST_ERROR, "Attribute list error" },
      { CipStatus.ALREADY_IN_REQUEST_STATE, "Already in request state" },
      { CipStatus.OBJECT_STATE_CONFLICT, "Object state conflict" },
      { CipStatus.OBJECT_ALREADY_EXISTS, "Object already exists" },
      { CipStatus.ATTRIBUTE_NOT_SETTABLE, "Attribute cannot be set" },
      { CipStatus.PRIVILEGE_VIOLATION, "Privilege violation" },
      { CipStatus.DEVICE_STATE_CONFLICT, "Device state conflict" },
      { CipStatus.RESPONSE_DATA_TOO_LARGE, "Response data is too large" },
      { CipStatus.FRAGMENTATION_OF_PRIMITIVE_VALUE, "Fragmentation of a primitive value" },
      { CipStatus.NOT_ENOUGH_DATA, "Not enough data" },
      { CipStatus.ATTRIBUTE_NOT_SUPPORTED, "Attribute is not supported" },
      { CipStatus.TOO_MUCH_DATA, "Too much data" },
      { CipStatus.OBJECT_NOT_EXISTS, "Object not exists" },
      { CipStatus.SEQUENCE_NOT_IN_PROGRESS, "Sequence number is not in progress" },
      { CipStatus.NO_STORED_ATTRIBUTE_DATA, "Attribute data is not stored" },
      { CipStatus.STORE_OPERATION_FAILURE, "Store operation failed" },
      { CipStatus.ROUTING_REQUEST_PACKET_TOO_LARGE, "Routing request message packet is too large" },
      { CipStatus.ROUTING_RESPONSE_PACKET_TOO_LARGE, "Routing response message packet is too large" },
      { CipStatus.MISSING_ATTRIBUTE_LIST_DATA, "Attribute list data is missing" },
      { CipStatus.INVALID_ATTRIBUTE_VALUE_LIST, "Invalid attribute list value" },
      { CipStatus.OPERATION_ERROR, "Operation error" },
      { CipStatus.VENDOR_SPECIFIC_ERROR, "Vendor specific error" },
      { CipStatus.INVALID_PARAMETER, "Invalid parameter" },
      { CipStatus.WRITE_ONCE_VALUE_ALREADY_WRITTEN, "Write-once value is already written" },
      { CipStatus.INVALID_RESPONSE, "Invalid response" },
      { CipStatus.BUFFER_OVERFLOW, "Buffer is in overflow" },
      { CipStatus.MESSAGE_FORMAT_ERROR, "Message format error" },
      { CipStatus.KEY_FAILURE_IN_PATH, "Kay failure in path" },
      { CipStatus.PATH_SIZE_INVALID, "Path size is invalid" },
      { CipStatus.UNEXPECTED_ATTRIBUTE_IN_LIST, "Unexpecetd attribute in the list" },
      { CipStatus.INVALID_MEMBER_ID, "Invalid member identifier" },
      { CipStatus.MEMBER_NOT_SETTABLE, "Member cannot be set" },
      { CipStatus.SEVER_GENERALE_FAILURE, "Sever general failure" },
      { CipStatus.UNKNOWN_MODBUS_ERROR, "Unknown modbus error" },
      { CipStatus.ATTRIBUTE_NOT_GETTABLE, "Attribute cannot be get" },
      { CipStatus.INSTANCE_NOT_DELETABLE, "Instance cannot be deleted" },
    };

    #region ATTRIBUTES

    /// <summary>
    /// CIP Response status code
    /// </summary>
    public CipStatus Status { get; set; }

    /// <summary>
    /// CIP Additional status code
    /// </summary>
    public ushort[] AdditionalStatus { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipException(CipStatus status, ushort[] additionalStatus = null) : base(Messages[status])
    {
      Status = status;
      AdditionalStatus = (additionalStatus is null) ? new ushort[0] : additionalStatus;
    }

    #endregion
  }
}
