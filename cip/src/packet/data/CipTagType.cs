﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         31-01-2020
 *  
 *  TITLE:        Cip Tag Type enum
 *  DESCRIPTION:  Specifies the atomic tag ID
 */

namespace Cip
{
  /// <summary>
  /// Specifies the atomic tag ID
  /// </summary>
  public enum CipTagType
  {
    BOOL = 0xC1,            //  boolean
    SINT = 0xC2,            //  byte
    INT = 0xC3,             //  short
    DINT = 0xC4,            //  int
    LINT = 0xC5,            //  long
    USINT = 0xC6,           //  byte
    UINT = 0xC7,            //  ushort
    UDINT = 0xC8,           //  uint
    ULINT = 0xC9,           //  ulong
    REAL = 0xCA,            //  float
    LREAL = 0xCB,           //  double
    STIME = 0xCC,
    DATE = 0xCD,
    TIME_OF_DAY = 0xCE,
    DATE_AND_TIME = 0x0A,   //  DateTime
    STRING = 0xD0,          //  string
    BYTE = 0xD1,            //  byte
    WORD = 0xD2,            //  ushort
    DWORD = 0xD3,           //  uint
    LWORD = 0xD4,           //  ulong
    STRING2 = 0xD5,
    FTIME = 0xD6,
    LTIME = 0xD7,
    ITIME = 0xD8,
    STRINGN = 0xD9,
    SHORT_STRING = 0xDA,
    TIME = 0xDB,
    EPATH = 0xDC,
    ENGUNIT = 0xDD,
    STRINGI = 0xDE
  }
}
