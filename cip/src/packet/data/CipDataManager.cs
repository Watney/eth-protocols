﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         14-05-2020
 *  
 *  TITLE:        Cip Data Manager class
 *  DESCRIPTION:  Specifies the atomic tag ID
 */

using UtilByte;
using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Cip
{
  /// <summary>
  /// Static class used to serialize and deserialize atomic, struct and array data
  /// </summary>
  public static class CipDataManager
  {
    const ushort CIP_STRUCT_TAG = 0x02A0;

    public static Dictionary<CipTagType, Type> Types = new Dictionary<CipTagType, Type>()
    {
      { CipTagType.BOOL,  typeof(bool)   },

      { CipTagType.SINT,  typeof(sbyte)  },
      { CipTagType.INT,   typeof(short)  },
      { CipTagType.DINT,  typeof(int)    },
      { CipTagType.LINT,  typeof(long)   },

      { CipTagType.USINT, typeof(byte)   },
      { CipTagType.UINT,  typeof(ushort) },
      { CipTagType.UDINT, typeof(uint)   },
      { CipTagType.ULINT, typeof(ulong)  },

      { CipTagType.BYTE,  typeof(byte)   },
      { CipTagType.WORD,  typeof(ushort) },
      { CipTagType.DWORD, typeof(uint)   },
      { CipTagType.LWORD, typeof(ulong)  },

      { CipTagType.REAL, typeof(float)   },
      { CipTagType.LREAL, typeof(double) },

      { CipTagType.STRING, typeof(string) },
      { CipTagType.DATE_AND_TIME, typeof(DateTime) }
    };
    public static Dictionary<Type, int> Sizes = new Dictionary<Type, int>()
    {
      { typeof(bool), 2},

      { typeof(sbyte), 1},
      { typeof(short), 2},
      { typeof(int), 4},
      { typeof(long), 8},

      { typeof(byte), 1},
      { typeof(ushort), 2},
      { typeof(uint), 4},
      { typeof(ulong), 8},

      { typeof(float), 4},
      { typeof(double), 8},

      { typeof(string), 0},
      { typeof(DateTime), 8}
    };

    #region REGEX FUNCTION TO GET REQUEST PATH FROM TAG AND VICEVERSA

    private static string CIP_REGEX = @"((?<text>[a-z|A-Z|_|0-9]+)(\[(?<index>[0-9]+)\])?(\.)?)";

    public static CipSegmentPath GetRequestPath(string variable)
    {
      Regex regex = new Regex(CIP_REGEX);
      MatchCollection matches = regex.Matches(variable);

      List<CipSegment> segments = new List<CipSegment>();

      foreach (Match match in matches)
      {
        GroupCollection groups = match.Groups;

        if (groups["text"].Success)
          segments.Add(new CipDataSegment(groups["text"].Value));

        if (groups["index"].Success)
          segments.Add(new CipLogicalSegment(uint.Parse(groups["index"].Value), CipLogicalType.Member, CipLogicalFormat.Bits8));
      }

      return new CipSegmentPath(segments.ToArray());
    }

    public static string GetTag(CipSegmentPath reqPath)
    {
      string tag = "";

      foreach (CipSegment segment in reqPath.Segments)
      {
        if (segment is CipDataSegment)
        {
          try
          {
            string txt = Encoding.UTF8.GetString((segment as CipDataSegment).Data);
            if (tag.Length > 0) tag += '.';
            tag += txt;
          }
          catch (Exception) { }
        }

        else if (segment is CipLogicalSegment)
          tag += string.Format("[{0}]", (segment as CipLogicalSegment).Value);
      }

      return tag;
    }

    #endregion

    #region MAIN FUNCTIONS

    /// <summary>
    /// Serialize the desired value in a byte array
    /// </summary>
    public static byte[] Serialize<T>(T value, ushort count = 1, bool isWriteOperation = true)
    {
      if (typeof(T).IsArray)
        return SerializeArray(value as Array, count, true, isWriteOperation);

      else
      {
        try
        {
          return SerializeAtomic(value, count, true, isWriteOperation);
        }
        catch (ArgumentNullException)
        {
          return SerializeStruct(value, true, isWriteOperation);
        }
      }
    }

    /// <summary>
    /// Serialize the desired value in a byte array
    /// </summary>
    public static byte[] Serialize(object value, bool writeTag = true, ushort count = 1, bool isWriteOperation = true)
    {
      if (value.GetType().IsArray)
        return SerializeArray(value as Array, count, writeTag, isWriteOperation);

      else
      {
        try
        {
          return SerializeAtomic(value, count, writeTag, isWriteOperation);
        }
        catch (ArgumentNullException)
        {
          return SerializeStruct(value, writeTag, isWriteOperation);
        }
      }
    }


    /// <summary>
    /// Deserialize a byte stream in the desired type
    /// </summary>
    public static T Deserialize<T>(MemoryStream stream, int length = 1, ushort count = 1, bool isWriteOperation = false)
    {
      if (length > 1)
        return (T)DeserializeArray(stream, typeof(T), length, count, true, isWriteOperation);

      else
      {
        try
        {
          return (T)DeserializeAtomic(stream, typeof(T), count, true, isWriteOperation);
        }
        catch (Exception ex)
        {
          if (ex is InvalidOperationException || ex is InvalidCastException)
            return (T)DeserializeStruct(stream, typeof(T), true, isWriteOperation);
          else
            throw ex;
        }
      }
    }

    /// <summary>
    /// Deserialize a byte stream in the desired type
    /// </summary>
    public static object Deserialize(MemoryStream stream, Type type, bool readTag = true, int length = 1, ushort count = 1, bool isWriteOperation = false)
    {
      if (length > 1)
        return DeserializeArray(stream, type, length, count, readTag, isWriteOperation);

      else
      {
        try
        {
          return DeserializeAtomic(stream, type, count, readTag, isWriteOperation);
        }
        catch (Exception ex)
        {
          if (ex is InvalidOperationException || ex is InvalidCastException)
            return DeserializeStruct(stream, type, readTag, isWriteOperation);
          else
            throw ex;
        }
      }
    }

    #endregion

    #region ATOMIC FUNCTIONS

    /// <summary>
    /// Function used to serialize an atomic value
    /// </summary>
    private static byte[] SerializeAtomic(object value, ushort count = 1, bool writeTag = true, bool isWriteOperation = true)
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      if (writeTag)
      {
        writer.Write((ushort)Types.FirstOrDefault(t => t.Value == value.GetType()).Key);

        if (isWriteOperation || value is string)
          writer.Write(count);
      }

      switch (Types.FirstOrDefault(t => t.Value == value.GetType()).Key)
      {
        case CipTagType.BOOL: writer.Write((bool)value); break;

        case CipTagType.SINT: writer.Write((sbyte)value); break;
        case CipTagType.INT: writer.Write((short)value); break;
        case CipTagType.DINT: writer.Write((int)value); break;
        case CipTagType.LINT: writer.Write((long)value); break;

        case CipTagType.USINT: writer.Write((byte)value); break;
        case CipTagType.UINT: writer.Write((ushort)value); break;
        case CipTagType.UDINT: writer.Write((uint)value); break;
        case CipTagType.ULINT: writer.Write((ulong)value); break;

        case CipTagType.REAL: writer.Write((float)value); break;
        case CipTagType.LREAL: writer.Write((double)value); break;

        case CipTagType.STRING: writer.Write(Encoding.UTF8.GetBytes(value as string)); break;
        case CipTagType.DATE_AND_TIME: writer.Write((ulong)((DateTime)value).Millisecond); break;

        default: throw new NotImplementedException("This Cip Tag is not implemented");
      }

      //  If the value to serialize is a string and it's in a struct request:
      //  write pad byte until its maximum size
      if (!writeTag && value is string)
      {
        for (int i = (value as string).Length; i <= count; i++)
          writer.Write((byte)0);
      }

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Function used to deserialize an atomic value
    /// </summary>
    private static object DeserializeAtomic(MemoryStream stream, Type type, ushort count = 1, bool readTag = true, bool isWriteOperation = true)
    {
      ByteReader reader = new ByteReader(stream);

      if (readTag)
      {
        if (!Types.Keys.Contains((CipTagType)reader.ReadUInt16()))
        {
          reader.Index -= 2;
          throw new InvalidCastException("Request value is of a different type of the value to be deserialized");
        }

        if (type == typeof(string) || isWriteOperation)
          count = reader.ReadUInt16();
      }

      switch (Types.First(t => t.Value == type).Key)
      {
        case CipTagType.BOOL: return reader.ReadBoolean();

        case CipTagType.SINT: return reader.ReadSByte();
        case CipTagType.INT: return reader.ReadInt16();
        case CipTagType.DINT: return reader.ReadInt32();
        case CipTagType.LINT: return reader.ReadInt64();

        case CipTagType.USINT: return reader.ReadByte();
        case CipTagType.UINT: return reader.ReadUInt16();
        case CipTagType.UDINT: return reader.ReadUInt32();
        case CipTagType.ULINT: return reader.ReadUInt64();

        case CipTagType.REAL: return reader.ReadSingle();
        case CipTagType.LREAL: return reader.ReadDouble();

        case CipTagType.STRING: return Encoding.UTF8.GetString(reader.ReadBytes(count)).Trim('\0');
        case CipTagType.DATE_AND_TIME: return new DateTime(reader.ReadInt64() / 1000000, DateTimeKind.Utc);

        default: throw new NotImplementedException("This Cip Tag is not implemented");
      }
    }

    #endregion

    #region STRUCT FUNCTIONS

    /// <summary>
    /// Function used to get the "CipStruct" attribute from a struct type
    /// </summary>
    public static CipStruct GetStruct(Type type)
    {
      //  Get Cip Struct informations (struct name and id) from the type requested 
      object[] attributes = type.GetCustomAttributes(true);
      return (CipStruct)attributes.ToList().Find((object attr) => (attr is CipStruct));
    }

    /// <summary>
    /// Function used to get all "CipProperties" from a struct type
    /// </summary>
    public static Dictionary<CipProperty, PropertyInfo> GetProperties(Type type)
    {
      Dictionary<CipProperty, PropertyInfo> properties = new Dictionary<CipProperty, PropertyInfo>();

      //  For each property of the structure obtain the "CipProperty" attribute (if the property has it)
      foreach (PropertyInfo info in type.GetProperties())
      {
        CipProperty prop = (CipProperty)info.GetCustomAttributes(true).ToList().Find((object attr) => (attr is CipProperty));

        if (prop != null)
          properties.Add(prop, info);
      }

      return properties;
    }


    /// <summary>
    /// Function used to serialize a struct
    /// </summary>
    private static byte[] SerializeStruct(object str, bool writeTag = true, bool isWriteOperation = true)
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      #region CHECK IF STRUCT INFORMATIONS MATCH

      CipStruct cipStr = GetStruct(str.GetType());
      Dictionary<CipProperty, PropertyInfo> properties = GetProperties(str.GetType());

      //  Check if the structure to serialize has the "CipStruct" attribute
      if (cipStr is null)
        throw new InvalidCastException("Cannot deserialize a struct without the \"CipStruct\" attribute");

      #endregion

      if (writeTag)
      {
        //  Write struct tag
        writer.Write(CIP_STRUCT_TAG);
        //  Write struct handle 
        writer.Write(cipStr.Id);

        //  Write request count (DEFAULT 1)
        if (isWriteOperation)
          writer.Write((ushort)1);
      }

      //  Write each property value of the struct
      foreach (CipProperty prop in properties.Keys.OrderBy((CipProperty p) => p.Order))
      {
        PropertyInfo info = properties[prop];
        byte[] propValue = Serialize(info.GetValue(str), false, 1, false);
        writer.Write(propValue);
      }

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Function used to deserialize a struct
    /// </summary>
    private static object DeserializeStruct(MemoryStream stream, Type type, bool readTag = true, bool isWriteOperation = true)
    {
      ByteReader reader = new ByteReader(stream);

      #region CHECK IF STRUCT INFORMATIONS MATCH

      CipStruct str = GetStruct(type);
      Dictionary<CipProperty, PropertyInfo> properties = GetProperties(type);

      //  Check if the structure to deserialize has the "CipStruct" attribute
      if (str is null)
        throw new InvalidCastException("Cannot deserialize a struct without the \"CipStruct\" attribute");

      if (readTag)
      {
        //  Check if the value to be deserialized is a struct
        if (reader.ReadUInt16() != CIP_STRUCT_TAG)
        {
          reader.Index -= 2;
          throw new InvalidCastException("The value to be deserialized is not a structure");
        }

        //  Check if the requested struct id match with the deserialized struct id
        ushort desId = reader.ReadUInt16();
        if (str.Id != desId)
        {
          reader.Index -= 2;
          throw new InvalidCastException(string.Format("Cannot convert an {0} struct in a {1} struct", desId, str.Id));
        }

        //  Read request count 
        ushort count = 1;
        if (isWriteOperation)
          count = reader.ReadUInt16();
      }

      #endregion

      //  Create an object of the desired type
      var result = Activator.CreateInstance(type);

      int size = str.Size;

      //  Each property (ordered by order number) of the structure is filled by its atomic value 
      foreach (CipProperty prop in properties.Keys.OrderBy((CipProperty p) => p.Order))
      {
        PropertyInfo info = properties[prop];
        object propValue = Deserialize(stream, info.PropertyType, false, prop.Length, (ushort)prop.Size, false);

        info.SetValue(result, propValue);
        size -= prop.Size;
      }

      //  Read padding bytes if they exist
      if (size > 0)
        reader.ReadBytes(size);

      return result;
    }

    #endregion

    #region ARRAY FUNCTIONS

    /// <summary>
    /// Function used to serialize an array value
    /// </summary>
    private static byte[] SerializeArray(Array arr, ushort count = 1, bool writeTag = true, bool isWriteOperation = true)
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());
      bool isStruct = false;

      //  Check if array is of struct or atomic values
      try
      {
        byte[] tmp = SerializeAtomic(arr.GetValue(0), count, writeTag, isWriteOperation);
        isStruct = false;
      }
      catch (ArgumentNullException)
      {
        byte[] tmp = SerializeStruct(arr.GetValue(0), writeTag, isWriteOperation);
        isStruct = true;
      }

      //  Write first element with TAG
      writer.Write(isStruct ? SerializeStruct(arr.GetValue(0)) : SerializeAtomic(arr.GetValue(0), count));
      //  Write others without
      for (int i = 1; i < arr.Length; i++)
        writer.Write(isStruct ? SerializeStruct(arr.GetValue(i), false, false) : SerializeAtomic(arr.GetValue(i), count, false, false));

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Function used to deserialize an array value
    /// </summary>
    private static object DeserializeArray(MemoryStream stream, Type type, int length, ushort count = 1, bool readTag = true, bool isWriteOperation = true)
    {
      object result = Activator.CreateInstance(type, new object[] { length });
      bool isStruct = false;

      //  Read first element with TAG
      try
      {
        (result as Array).SetValue(DeserializeAtomic(stream, type.GetElementType(), count, readTag, isWriteOperation), 0);
        isStruct = false;
      }
      catch (Exception ex)
      {
        if (ex is InvalidCastException || ex is InvalidOperationException)
        {
          (result as Array).SetValue(DeserializeStruct(stream, type.GetElementType(), readTag, isWriteOperation), 0);
          isStruct = true;
        }
        else
          throw ex;
      }

      //  Read others without
      for (int i = 1; i < length; i++)
      {
        if (isStruct)
          (result as Array).SetValue(DeserializeStruct(stream, type.GetElementType(), false, false), i);
        else
          (result as Array).SetValue(DeserializeAtomic(stream, type.GetElementType(), count, false, false), i);
      }

      return result;
    }

    #endregion
  }
}
