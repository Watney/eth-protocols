﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         19-02-2020
 *  
 *  TITLE:        Cip Struct and Property attributes
 *  DESCRIPTION:  Used to serialize and deserialize custom structures
 */

using System;

namespace Cip
{
  /// <summary>
  /// Attribute used to serialize and deserialize structures
  /// </summary>
  [AttributeUsage(AttributeTargets.Class)]
  public class CipStruct : Attribute
  {
    #region ATTRIBUTES

    /// <summary>
    /// Symbolic name of the structure
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Identifier code of the structure (used to write an entire structure)
    /// </summary>
    public ushort Id { get; set; }

    /// <summary>
    /// Specifies the byte size of the structure 
    /// </summary>
    public int Size { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipStruct(string name, ushort id, int size)
    {
      Name = name;
      Id = id;
      Size = size;
    }

    #endregion
  }

  /// <summary>
  /// Attribute used to serialize and deserialize structure's properties
  /// </summary>
  [AttributeUsage(AttributeTargets.Property)]
  public class CipProperty : Attribute
  {
    #region ATTRIBUTES

    /// <summary>
    /// Symbolic name of the property
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Serialization order number
    /// </summary>
    public ushort Order { get; set; }

    /// <summary>
    /// Specifies the byte size of the property (optional) 
    /// </summary>
    public int Size { get; set; }

    /// <summary>
    /// Specifies the number ot items of the property (optional)(if it's array) 
    /// </summary>
    public int Length { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipProperty(string name, ushort order, int size = -1, int length = 1)
    {
      Name = name;
      Order = order;
      Size = size;
      Length = length;
    }

    #endregion
  }
}
