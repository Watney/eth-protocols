﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         12-02-2020
 *  
 *  TITLE:        Cip Message Router Request object
 *  DESCRIPTION:  Object used to encapsulate request messages
 */

using UtilByte;
using System;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Object used to encapsulate request messages
  /// </summary>
  public class CipMessageRouterRequest : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// Service identifier
    /// </summary>
    public byte Service { get; set; }

    /// <summary>
    /// Request path
    /// </summary>
    public CipSegmentPath Path { get; set; }

    /// <summary>
    /// Request data
    /// </summary>
    public byte[] Data { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipMessageRouterRequest(byte service, CipSegmentPath path, byte[] data = null)
    {
      Service = service;
      Path = path;
      Data = (data is null) ? new byte[0] : data;
    }

    public CipMessageRouterRequest(MemoryStream stream)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Message Router Request in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(Service);
      writer.Write(Path.Serialize());
      writer.Write(Data);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Message Router Request 
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Service = reader.ReadByte();
      Path = new CipSegmentPath(stream);
      Data = reader.ReadBytes((int)(reader.BaseStream.Length - reader.Index));
    }

    #endregion
  }
}
