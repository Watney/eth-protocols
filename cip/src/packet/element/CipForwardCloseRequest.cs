﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cip Forward Close Request object
 *  DESCRIPTION:  Object used to send a close request message in a connected communication
 */

using UtilByte;
using System;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Object used to send a close request message in a connected communication
  /// </summary>
  public class CipForwardCloseRequest : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// Timeout interval of the connection
    /// </summary>
    public CipTimeout ConnectionTimeout { get; set; }

    /// <summary>
    /// Connection identifier number
    /// </summary>
    public ushort ConnectionNumber { get; set; }

    /// <summary>
    /// Vendor identifier
    /// </summary>
    public ushort VendorID { get; set; }

    /// <summary>
    /// Device serial number
    /// </summary>
    public uint SerialNumber { get; set; }

    /// <summary>
    /// Connection path
    /// </summary>
    public CipSegmentPath ConnectionPath { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipForwardCloseRequest(CipTimeout connTimeout, ushort connNumber, ushort vendor, uint serialNumber, CipSegmentPath connPath)
    {
      ConnectionTimeout = connTimeout;
      ConnectionNumber = connNumber;
      VendorID = vendor;
      SerialNumber = serialNumber;
      ConnectionPath = connPath;
    }

    public CipForwardCloseRequest(MemoryStream stream)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Forward Close Request in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(ConnectionTimeout.TickTimer);
      writer.Write(ConnectionTimeout.Ticks);

      writer.Write(ConnectionNumber);
      writer.Write(VendorID);
      writer.Write(SerialNumber);

      #region SERIALIZE CONNECTION PATH

      byte[] connPath = ConnectionPath.Serialize();

      writer.Write(connPath[0]);
      writer.Write((byte)0);
      writer.Write(connPath, 1, connPath.Length - 1);

      #endregion

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Forward Close Request
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      ConnectionTimeout = new CipTimeout(reader.ReadByte(), reader.ReadByte());

      ConnectionNumber = reader.ReadUInt16();
      VendorID = reader.ReadUInt16();
      SerialNumber = reader.ReadUInt32();

      #region DESERIALIZE CONNECTION PATH 

      byte length = reader.ReadByte();
      byte[] connPath = reader.ReadBytes((length * 2) + 1);
      connPath[0] = length;

      ConnectionPath = new CipSegmentPath(new MemoryStream(connPath));

      #endregion
    }

    #endregion
  }
}
