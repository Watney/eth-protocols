﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cip Forward Close Response object
 *  DESCRIPTION:  Object used to send a close response message in a connected communication
 */

using UtilByte;
using System;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Object used to send a close response message in a connected communication
  /// </summary>
  public class CipForwardCloseResponse : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// Connection identifier number
    /// </summary>
    public ushort ConnectionNumber { get; set; }

    /// <summary>
    /// Vendor identifier
    /// </summary>
    public ushort VendorID { get; set; }

    /// <summary>
    /// Device serial number
    /// </summary>
    public uint SerialNumber { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipForwardCloseResponse(ushort connNumber, ushort vendor, uint serialNumber)
    {
      ConnectionNumber = connNumber;
      VendorID = vendor;
      SerialNumber = serialNumber;
    }

    public CipForwardCloseResponse(MemoryStream stream)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Forward Close Response in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(ConnectionNumber);
      writer.Write(VendorID);
      writer.Write(SerialNumber);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Forward Close Request
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      ConnectionNumber = reader.ReadUInt16();
      VendorID = reader.ReadUInt16();
      SerialNumber = reader.ReadUInt32();
    }

    #endregion
  }
}
