﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cip Forward Open Response object
 *  DESCRIPTION:  Object used to send an open response message in a connected communication
 */

using UtilByte;
using System;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Object used to send an open response message in a connected communication
  /// </summary>
  public class CipForwardOpenResponse : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// Target to Originator connection identifier
    /// </summary>
    public uint TOConnectionID { get; set; }

    /// <summary>
    /// Originator to Target connection identifier
    /// </summary>
    public uint OTConnectionID { get; set; }

    /// <summary>
    /// Connection identifier number
    /// </summary>
    public ushort ConnectionNumber { get; set; }

    /// <summary>
    /// Device vendor identifier
    /// </summary>
    public ushort VendorID { get; set; }

    /// <summary>
    /// Device vendor serial number
    /// </summary>
    public uint VendorSerialNumber { get; set; }

    /// <summary>
    /// Target to Originator actual packet interval
    /// </summary>
    public uint TOApi { get; set; }

    /// <summary>
    /// Originator to Target actual packet interval
    /// </summary>
    public uint OTApi { get; set; }

    /// <summary>
    /// Application response data
    /// </summary>
    public byte[] ApplicationResponse { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipForwardOpenResponse(uint toConnId, uint otConnId, ushort connNumber, ushort vendor, uint vendorSerialNumber, uint toApi, uint otApi, byte[] appResponse)
    {
      TOConnectionID = toConnId;
      OTConnectionID = otConnId;
      ConnectionNumber = connNumber;
      VendorID = vendor;
      VendorSerialNumber = vendorSerialNumber;
      TOApi = toApi;
      OTApi = otApi;
      ApplicationResponse = appResponse;
    }

    public CipForwardOpenResponse(MemoryStream stream)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Forward Open Response in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(OTConnectionID);
      writer.Write(TOConnectionID);
      writer.Write(ConnectionNumber);

      writer.Write(VendorID);
      writer.Write(VendorSerialNumber);

      writer.Write(OTApi);
      writer.Write(TOApi);

      writer.Write((byte)ApplicationResponse.Length);
      writer.Write((byte)0); //  Reserved byte
      writer.Write(ApplicationResponse);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Forward Open Response
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      OTConnectionID = reader.ReadUInt32();
      TOConnectionID = reader.ReadUInt32();
      ConnectionNumber = reader.ReadUInt16();

      VendorID = reader.ReadUInt16();
      VendorSerialNumber = reader.ReadUInt32();

      OTApi = reader.ReadUInt32();
      TOApi = reader.ReadUInt32();

      byte appResponseLength = reader.ReadByte();
      reader.ReadByte(); //  Reserved byte
      ApplicationResponse = reader.ReadBytes(appResponseLength);
    }

    #endregion
  }
}
