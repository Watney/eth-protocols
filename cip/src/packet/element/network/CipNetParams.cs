﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         24-01-2020
 *  
 *  TITLE:        Cip Network Parameters object
 *  DESCRIPTION:  Object used to send network informations about sender/receiver
 */

namespace Cip
{
  /// <summary>
  /// Object used to send network informations about sender/receiver
  /// </summary>
  public class CipNetParams
  {
    #region ATTRIBUTES

    /// <summary>
    /// Byte size of the connection
    /// </summary>
    public uint ConnectionSize { get; set; }

    /// <summary>
    /// Size format of the packet
    /// </summary>
    public CipNetSizeType SizeType { get; set; }

    /// <summary>
    /// Priority level of the packet
    /// </summary>
    public CipNetPriority Priority { get; set; }

    /// <summary>
    /// Connection used in the communication
    /// </summary>
    public CipNetConnectionType ConnectionType { get; set; }

    /// <summary>
    /// Flag that indicates if the sender is the receiver
    /// </summary>
    public bool RedundantOwner { get; set; }

    /// <summary>
    /// Flag that indicates if the request service is Large Forward Open or Forward Open
    /// </summary>
    public bool IsLargeRequest { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipNetParams(uint connSize, CipNetSizeType sizeType, CipNetPriority priority, CipNetConnectionType connType, bool redundantOwner, bool isLargeRequest)
    {
      ConnectionSize = connSize;
      SizeType = sizeType;
      Priority = priority;
      ConnectionType = connType;
      RedundantOwner = redundantOwner;
      IsLargeRequest = IsLargeRequest;
    }

    #endregion

    #region DEFINE CONVERSION OPERATORS FROM/TO USHORT AND UINT TYPES

    public static explicit operator ushort(CipNetParams param)
    {
      uint val = param.ConnectionSize & 0x1FF;            //  0   00   0   00   0   0  [00000000]

      val |= (uint)param.SizeType << 9;                   //  0   00   0   00  [0]  0   00000000

      val |= (uint)param.Priority << 10;                  //  0   00   0  [00]  0   0   00000000

      val |= (uint)param.ConnectionType << 13;            //  0  [00]  0   00   0   0   00000000

      if (param.RedundantOwner) val |= (uint)1 << 15;     // [0]  00   0   00   0   0   00000000

      return (ushort)val;
    }

    public static explicit operator uint(CipNetParams param)
    {
      uint val = param.ConnectionSize & 0xFFFF;           //  0   00   0   00   0   0  [00000000 ...]

      val |= (uint)param.SizeType << 25;                  //  0   00   0   00  [0]  0   00000000 ...

      val |= (uint)param.Priority << 26;                  //  0   00   0  [00]  0   0   00000000 ...

      val |= (uint)param.ConnectionType << 29;            //  0  [00]  0   00   0   0   00000000 ...

      if (param.RedundantOwner) val |= (uint) 1 << 31;    // [0]  00   0   00   0   0   00000000 ...

      return val;
    }

    public static explicit operator CipNetParams(ushort param)
    {
      return new CipNetParams
      (
        (uint)(param & 0xFF),
        (CipNetSizeType)((param >> 9) & 0x1),
        (CipNetPriority)((param >> 10) & 0x3),
        (CipNetConnectionType)((param >> 13) & 0x3),
        (param >> 15) == 1,
        false
      );
    }

    public static explicit operator CipNetParams(uint param)
    {
      return new CipNetParams
      (
        param & 0xFFF,
        (CipNetSizeType)((param >> 25) & 0x1),
        (CipNetPriority)((param >> 26) & 0x3),
        (CipNetConnectionType)((param >> 29) & 0x3),
        (param >> 31) == 1,
        true
      );
    }

    #endregion
  }
}
