﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         22-01-2020
 *  
 *  TITLE:        Cip Network Priority enum
 *  DESCRIPTION:  Specifies the priority level of the packet
 */

namespace Cip
{
  /// <summary>
  /// Specifies the priority level of the packet
  /// </summary>
  public enum CipNetPriority
  {
    Low,
    High,
    Scheduled,
    Urgent
  }
}
