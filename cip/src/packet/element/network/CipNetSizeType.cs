﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         22-01-2020
 *  
 *  TITLE:        Cip Network Size Type enum
 *  DESCRIPTION:  Specifies the packet size format
 */

namespace Cip
{
  /// <summary>
  /// Specifies the size format
  /// </summary>
  public enum CipNetSizeType
  {
    Fixed,
    Variable
  }
}
