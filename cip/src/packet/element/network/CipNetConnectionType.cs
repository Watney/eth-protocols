﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         22-01-2020
 *  
 *  TITLE:        Cip Network Connection Type enum
 *  DESCRIPTION:  Specifies the connection used in the communication
 */

namespace Cip
{
  /// <summary>
  /// Specifies the connection used in the communication
  /// </summary>
  public enum CipNetConnectionType
  {
    Null,
    Multicast,
    PointToPoint,
    Reserved
  }
}
