﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         12-02-2020
 *  
 *  TITLE:        Cip Message Router Response object
 *  DESCRIPTION:  Object used to encapsulate response messages
 */

using System;
using UtilByte;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Object used to encapsulate response messages
  /// </summary>
  public class CipMessageRouterResponse : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// Service identifier
    /// </summary>
    public byte Service { get; set; }

    /// <summary>
    /// CIP Response status code
    /// </summary>
    public CipStatus Status { get; set; }

    /// <summary>
    /// CIP Additional status code
    /// </summary>
    public ushort[] AdditionalStatus { get; set; }

    /// <summary>
    /// Response data
    /// </summary>
    public byte[] Data { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipMessageRouterResponse(byte service, byte[] data, CipStatus status, ushort[] additionalStatus = null)
    {
      Service = service;
      Data = data;
      Status = status;
      AdditionalStatus = (additionalStatus is null) ? new ushort[0] : additionalStatus;
    }

    public CipMessageRouterResponse(MemoryStream stream)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Message Router Response in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(Service);
      writer.Write((byte)0); // Reserved
      writer.Write((byte)Status);
      writer.Write((byte)AdditionalStatus.Length);

      foreach (uint val in AdditionalStatus)
        writer.Write(val);

      writer.Write(Data);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Message Router Response 
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Service = reader.ReadByte();
      byte reserved = reader.ReadByte();

      Status = (CipStatus)reader.ReadByte();
      AdditionalStatus = new ushort[reader.ReadByte()];

      for (int i = 0; i < AdditionalStatus.Length; i++)
        AdditionalStatus[i] = reader.ReadUInt16();

      Data = reader.ReadBytes((int)(reader.BaseStream.Length - reader.Index));
    }

    #endregion
  }
}
