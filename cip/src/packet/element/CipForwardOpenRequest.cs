﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cip Forward Open Request object
 *  DESCRIPTION:  Object used to send an open request message in a connected communication
 */

using UtilByte;
using System;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Object used to send an open request message in a connected communication
  /// </summary>
  public class CipForwardOpenRequest : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// Timeout interval of the connection
    /// </summary>
    public CipTimeout Timeout { get; set; }

    /// <summary>
    /// Target to Originator connection identifier
    /// </summary>
    public uint TOConnectionID { get; set; }

    /// <summary>
    /// Originator to Target connection identifier
    /// </summary>
    public uint OTConnectionID { get; set; }

    /// <summary>
    /// Connection identifier number
    /// </summary>
    public ushort ConnectionNumber { get; set; }

    /// <summary>
    /// Device vendor identifier
    /// </summary>
    public ushort VendorID { get; set; }

    /// <summary>
    /// Device vendor serial number
    /// </summary>
    public uint VendorSerialNumber { get; set; }

    /// <summary>
    /// Connection timeout multiplier
    /// </summary>
    public byte ConnectionTimeoutMultiplier { get; set; }

    /// <summary>
    /// Connection path
    /// </summary>
    public CipSegmentPath ConnectionPath { get; set; }

    /// <summary>
    /// Target to Originator request packet interval
    /// </summary>
    public uint TORpi { get; set; }

    /// <summary>
    /// Target to Originator network parameters
    /// </summary>
    public CipNetParams TOParams { get; set; }

    /// <summary>
    /// Originator to Target request packet interval
    /// </summary>
    public uint OTRpi { get; set; }

    /// <summary>
    /// Originator to Target network parameters
    /// </summary>
    public CipNetParams OTParams { get; set; }

    /// <summary>
    /// Transport class and trigger
    /// </summary>
    public byte TransportClassAndTrigger { get; set; }

    /// <summary>
    /// Flag that indicates if use 2 or 4 bytes to serialize the Cip Network Parameters
    /// </summary>
    public bool IsLarge { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipForwardOpenRequest(CipTimeout timeout, uint toConnId, ushort connNumber, ushort vendor, uint vendorSerialNumber, byte connTimeoutMultiplier,
      CipSegmentPath connPath, uint toRpi, CipNetParams toParams, uint otRpi, CipNetParams otParams, byte transportClassTrigger, bool isLarge = false)
    {
      Timeout = timeout;
      TOConnectionID = toConnId;
      OTConnectionID = 0;
      ConnectionNumber = connNumber;
      VendorID = vendor;
      VendorSerialNumber = vendorSerialNumber;
      ConnectionTimeoutMultiplier = ConnectionTimeoutMultiplier;
      ConnectionPath = connPath;
      TORpi = toRpi;
      TOParams = toParams;
      OTRpi = otRpi;
      OTParams = otParams;
      TransportClassAndTrigger = transportClassTrigger;
      IsLarge = isLarge;
    }

    public CipForwardOpenRequest(MemoryStream stream, bool isLarge = false)
    {
      IsLarge = isLarge;
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Forward Open Request in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(Timeout.TickTimer);
      writer.Write(Timeout.Ticks);

      writer.Write(OTConnectionID); //  Must be 0
      writer.Write(TOConnectionID);
      writer.Write(ConnectionNumber);

      writer.Write(VendorID);
      writer.Write(VendorSerialNumber);

      writer.Write(ConnectionTimeoutMultiplier);
      writer.Write(new byte[] { 0, 0, 0 }); //  Write 3 reserved bytes

      writer.Write(OTRpi);

      if(IsLarge)
        writer.Write((uint)OTParams);
      else
        writer.Write((ushort)OTParams);

      writer.Write(TORpi);

      if (IsLarge)
        writer.Write((uint)TOParams);
      else
        writer.Write((ushort)TOParams);

      writer.Write(TransportClassAndTrigger);

      writer.Write(ConnectionPath.Serialize());

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Forward Open Request
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Timeout = new CipTimeout(reader.ReadByte(), reader.ReadByte());

      OTConnectionID = reader.ReadUInt32();
      TOConnectionID = reader.ReadUInt32();
      ConnectionNumber = reader.ReadUInt16();

      VendorID = reader.ReadUInt16();
      VendorSerialNumber = reader.ReadUInt32();

      ConnectionTimeoutMultiplier = reader.ReadByte();
      reader.ReadBytes(3);  //Read 3 reserved bytes

      OTRpi = reader.ReadUInt32();
      OTParams = (IsLarge) ? (CipNetParams)reader.ReadUInt32() : (CipNetParams)reader.ReadUInt16();

      TORpi = reader.ReadUInt32();
      TOParams = (IsLarge) ? (CipNetParams)reader.ReadUInt32() : (CipNetParams)reader.ReadUInt16();

      TransportClassAndTrigger = reader.ReadByte();

      ConnectionPath = new CipSegmentPath(stream);
    }

    #endregion
  }
}
