﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cip Attribute Request object
 *  DESCRIPTION:  Object used to set data to an attibute
 */

using UtilByte;
using System;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Object used to set data to an attibute
  /// </summary>
  public class CipAttributeRequest : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// The identifier code of the attribute requested
    /// </summary>
    public ushort Id { get; set; }

    /// <summary>
    /// Data to assign to the attribute requested
    /// </summary>
    public byte[] Data { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipAttributeRequest(ushort id, byte[] data)
    {
      Id = id;
      Data = data;
    }

    public CipAttributeRequest(MemoryStream stream)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Attribute Request object in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(Id);
      writer.Write(Data);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Attribute Request object
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Id = reader.ReadUInt16();
      Data = reader.ReadBytes((int)(reader.BaseStream.Length - reader.Index));
    }

    #endregion
  }
}
