﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cip Attribute Response object
 *  DESCRIPTION:  Object used to get data from an attibute
 */

using UtilByte;
using System;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Object used to get data from an attibute
  /// </summary>
  public class CipAttributeResponse : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// The identifier code of the attribute responsed 
    /// </summary>
    public ushort Id { get; set; }

    /// <summary>
    /// Status code of the response element
    /// </summary>
    public ushort Status { get; set; }

    /// <summary>
    /// Value data of the attribute responsed
    /// </summary>
    public byte[] Data { get; set; }

    /// <summary>
    /// Byte size of data field
    /// </summary>
    public ushort DataLength { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipAttributeResponse(ushort id, ushort status, byte[] data)
    {
      Id = id;
      Status = status;
      Data = data;
      DataLength = (ushort)Data.Length;
    }

    public CipAttributeResponse(MemoryStream stream, ushort dataLength)
    {
      DataLength = dataLength;
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Attribute Response object in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(Id);
      writer.Write(Status);
      writer.Write(Data);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Attribute Response object
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Id = reader.ReadUInt16();
      Status = reader.ReadUInt16();
      Data = (Status != 0) ? new byte[0] : reader.ReadBytes(DataLength);
    }

    #endregion
  }
}