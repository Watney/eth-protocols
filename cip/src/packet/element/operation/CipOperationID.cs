﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         27-01-2020
 *  
 *  TITLE:        Cip Operation ID enum
 *  DESCRIPTION:  Specifies the operation identifier code
 */

namespace Cip
{
  /// <summary>
  /// Specifies the operation identifier code
  /// </summary>
  public enum CipOperationID
  {
    ReadTag = 0x4C,
    WriteTag = 0x4D,
    ReadModifyWriteTag = 0x4E,
    ReadTagFragmented = 0x52,
    WriteTagFragmented = 0x53
  }
}
