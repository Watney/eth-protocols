﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cip Operation object
 *  DESCRIPTION:  Object used to encapsulate embedded service packets
 */

using UtilByte;
using System;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Object used to encapsulate embedded service packets
  /// </summary>
  public class CipOperation : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// Operation identifier code
    /// </summary>
    public CipOperationID Id { get; set; }

    /// <summary>
    /// Request path object used in the operation requested
    /// </summary>
    public CipSegmentPath RequestPath { get; set; }

    /// <summary>
    /// Optional data used in the operation requested
    /// </summary>
    public byte[] RequestData { get; set; }

    /// <summary>
    /// Specifies how many bytes read on deserializing
    /// </summary>
    public int Length { get; private set; }

    #endregion

    #region CONSTRUCTORS

    public CipOperation(CipOperationID id, CipSegmentPath reqPath, byte[] data = null)
    {
      Id = id;
      RequestPath = reqPath;
      RequestData = (data is null) ? new byte[0] : data;
    }

    public CipOperation(MemoryStream stream, int length)
    {
      Length = length;
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Cip Operation object in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((byte)Id);
      writer.Write(RequestPath.Serialize());
      writer.Write(RequestData);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Cip Operation object
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Id = (CipOperationID)reader.ReadByte();
      RequestPath = new CipSegmentPath(stream);
      RequestData = reader.ReadBytes(Length - (RequestPath.Serialize().Length + 1));
    }

    #endregion
  }
}
