﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         23-01-2020
 *  
 *  TITLE:        Cip Timeout object
 *  DESCRIPTION:  Object used to encapsulate timeout duration in CIP format
 */

namespace Cip
{
  /// <summary>
  /// Object used to encapsulate timeout duration in CIP format
  /// </summary>
  public class CipTimeout
  {
    #region ATTRIBUTES

    /// <summary>
    /// Interval timer between two ticks
    /// </summary>
    public byte TickTimer { get; set; }

    /// <summary>
    /// Number of ticks emitted
    /// </summary>
    public byte Ticks { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipTimeout(byte tickTimer, byte ticks)
    {
      TickTimer = tickTimer;
      Ticks = ticks;
    }

    #endregion

    #region DEFINE CONVERSION OPERATORS FROM/TO USHORT TYPE

    /*
     *  Timeout = ( 2 ^ TickTimer) * Ticks 
     * 
     *  Ticks = Timeout / ( 2 ^ TickTimer)
     * 
     *  TickTimer = log( Timeout / Ticks)
     */

    public static explicit operator ushort(CipTimeout timeout)
    {
      return (ushort)((1 << timeout.TickTimer) * timeout.Ticks);
    }

    public static explicit operator CipTimeout(ushort timeout)
    {
      byte idx = 0;

      while(timeout > byte.MaxValue)
      { 
        timeout /= 2;
        idx++;
      }

      return new CipTimeout(idx, (byte)timeout);
    }

    #endregion
  }
}
