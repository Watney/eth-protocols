﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         24-01-2020
 *  
 *  TITLE:        Cip Status enum
 *  DESCRIPTION:  Specifies the response status code
 */

namespace Cip
{
  /// <summary>
  /// Specifies the response status code
  /// </summary>
  public enum CipStatus
  {
    SUCCESS = 0x00,
    CONNECTION_FAILURE = 0x01,
    RESOURCE_UNAVAILABLE = 0x02,
    INVALID_PARAMETER_VALUE = 0x03,
    PATH_SEGMENT_ERROR = 0x04,
    PATH_DESTINATION_UNKNOWN = 0x05,
    PARTIAL_TRANSFER = 0x06,
    CONNECTION_LOST = 0x07,
    SERVICE_NOT_SUPPORTED = 0x08,
    INVALID_ATTRIBUTE_VALUE = 0x09,
    ATTRIBUTE_LIST_ERROR = 0x0A,
    ALREADY_IN_REQUEST_STATE = 0x0B,
    OBJECT_STATE_CONFLICT = 0x0C,
    OBJECT_ALREADY_EXISTS = 0x0D,
    ATTRIBUTE_NOT_SETTABLE = 0x0E,
    PRIVILEGE_VIOLATION = 0x0F,
    DEVICE_STATE_CONFLICT = 0x10,
    RESPONSE_DATA_TOO_LARGE = 0x11,
    FRAGMENTATION_OF_PRIMITIVE_VALUE = 0x12,
    NOT_ENOUGH_DATA = 0x13,
    ATTRIBUTE_NOT_SUPPORTED = 0x14,
    TOO_MUCH_DATA = 0x15,
    OBJECT_NOT_EXISTS = 0x16,
    SEQUENCE_NOT_IN_PROGRESS = 0x17,
    NO_STORED_ATTRIBUTE_DATA = 0x18,
    STORE_OPERATION_FAILURE = 0x19,
    ROUTING_REQUEST_PACKET_TOO_LARGE = 0x1A,
    ROUTING_RESPONSE_PACKET_TOO_LARGE = 0x1B,
    MISSING_ATTRIBUTE_LIST_DATA = 0x1C,
    INVALID_ATTRIBUTE_VALUE_LIST = 0x1D,
    OPERATION_ERROR = 0x1E,
    VENDOR_SPECIFIC_ERROR = 0x1F,
    INVALID_PARAMETER = 0x20,
    WRITE_ONCE_VALUE_ALREADY_WRITTEN = 0x21,
    INVALID_RESPONSE = 0x22,
    BUFFER_OVERFLOW = 0x23,
    MESSAGE_FORMAT_ERROR = 0x24,
    KEY_FAILURE_IN_PATH = 0x25,
    PATH_SIZE_INVALID = 0x26,
    UNEXPECTED_ATTRIBUTE_IN_LIST = 0x27,
    INVALID_MEMBER_ID = 0x28,
    MEMBER_NOT_SETTABLE = 0x29,
    SEVER_GENERALE_FAILURE = 0x2A,
    UNKNOWN_MODBUS_ERROR = 0x2B,
    ATTRIBUTE_NOT_GETTABLE = 0x2C,
    INSTANCE_NOT_DELETABLE = 0x2D
  }
}
