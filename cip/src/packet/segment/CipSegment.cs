﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         23-01-2020
 *  
 *  TITLE:        Cip Segment object
 *  DESCRIPTION:  Generic CIP segment object used in CIP packet
 */

using UtilByte;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Generic CIP segment object used in CIP packet
  /// </summary>
  public abstract class CipSegment : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// Segment type code
    /// </summary>
    public CipSegmentID Id { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipSegment(CipSegmentID id)
    {
      Id = id;
    }

    #endregion

    #region IMPLEMENT INTERFACE

    public abstract byte[] Serialize();

    public abstract void Deserialize(MemoryStream stream);

    #endregion
  }
}
