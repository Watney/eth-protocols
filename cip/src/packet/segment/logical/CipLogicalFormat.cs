﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         20-01-2020
 *  
 *  TITLE:        Cip Logical Format enum
 *  DESCRIPTION:  List of CIP logical segment's formats
 */

namespace Cip
{
  /// <summary>
  /// List of CIP logical segment's formats
  /// </summary>
  public enum CipLogicalFormat
  {
    Bits8 = 0x0,
    Bits16 = 0x1,
    Bits32 = 0x2,
    Reserved = 0x3
  }
}
