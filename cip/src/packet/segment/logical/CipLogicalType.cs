﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         20-01-2020
 *  
 *  TITLE:        Cip Logical Type enum
 *  DESCRIPTION:  List of CIP logical segment's types
 */

namespace Cip
{
  /// <summary>
  /// List of CIP logical segment's types
  /// </summary>
  public enum CipLogicalType
  {
    Class = 0x0,
    Instance = 0x1,
    Member = 0x2,
    ConnectionPoint = 0x3,
    Attribute = 0x4,
    Special = 0x5,
    Service = 0x6,
    Reserved = 0x7
  }
}
