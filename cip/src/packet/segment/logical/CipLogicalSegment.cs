﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         29-01-2020
 *  
 *  TITLE:        Cip Logical Segment object
 *  DESCRIPTION:  Segment used to represent classes, instances, attributes, services, members, connection points
 */

using UtilByte;
using System;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Segment used to represent classes, instances, attributes, services, members, connection points
  /// </summary>
  public class CipLogicalSegment : CipSegment
  {
    #region ATTRIBUTES

    /// <summary>
    /// Indicates the value type represent in this segment
    /// </summary>
    public CipLogicalType Type { get; set; }

    /// <summary>
    /// Indicates ho many bytes are used to represent the value in this segment
    /// </summary>
    public CipLogicalFormat Format { get; set; }

    /// <summary>
    /// Value represented in this segment
    /// </summary>
    public uint Value { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipLogicalSegment(uint value, CipLogicalType type) : this(value, type, CipLogicalFormat.Reserved)
    {
      if ((value & 0xFF) == value)
        Format = CipLogicalFormat.Bits8;

      else if ((value & 0xFFFF) == value)
        Format = CipLogicalFormat.Bits16;

      else
        Format = CipLogicalFormat.Bits32;
    }

    public CipLogicalSegment(uint value, CipLogicalType type, CipLogicalFormat format) : base(CipSegmentID.Logical)
    {
      Value = value;
      Type = type;
      Format = format;
    }

    public CipLogicalSegment(MemoryStream stream) : base(CipSegmentID.Logical)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Logical Segment in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      #region SERIALIZE SEGMENT ID

      int segId = ((int)Id) << 5;
      segId |= ((int)Type) << 2;
      segId |= (int)Format;

      writer.Write((byte)segId);

      #endregion  

      //  Add pad byte
      if (Format == CipLogicalFormat.Bits16 || Format == CipLogicalFormat.Bits32)
        writer.Write((byte)0);

      switch(Format)
      {
        case CipLogicalFormat.Bits8: writer.Write((byte)Value); break;
        case CipLogicalFormat.Bits16: writer.Write((ushort)Value); break;
        case CipLogicalFormat.Bits32: writer.Write(Value); break;
        case CipLogicalFormat.Reserved:
        default: throw new InvalidCastException("Segment format not supported");
      }

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Logical Segment
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      #region DESERIALIZE SEGMENT ID

      byte segId = reader.ReadByte();

      Id = (CipSegmentID)(segId >> 5);                // [000]  000   00

      if (Id != CipSegmentID.Logical)
        throw new InvalidCastException("Invalid segment identifier for a Logical Segment");

      Type = (CipLogicalType)((segId & 0x1c) >> 2);   //  000  [000]  00
      Format = (CipLogicalFormat)(segId & 0x03);      //  000   000  [00]

      #endregion

      bool padded = (Format == CipLogicalFormat.Bits16 || Format == CipLogicalFormat.Bits32) &&
                 (reader.ReadByte() == 0) && ((reader.BaseStream.Length - reader.Index) % 2 == 0);

      if (!padded)
        reader.Index--;

      switch(Format)
      {
        case CipLogicalFormat.Bits8: Value = reader.ReadByte(); break;
        case CipLogicalFormat.Bits16: Value = reader.ReadUInt16(); break;
        case CipLogicalFormat.Bits32: Value = reader.ReadUInt32(); break;
        case CipLogicalFormat.Reserved:
        default: throw new InvalidCastException("Segment format not supported");
      }
    }

    #endregion
  }
}
