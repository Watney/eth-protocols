﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         21-01-2020
 *  
 *  TITLE:        Cip Data Type enum
 *  DESCRIPTION:  List of CIP data segment's types
 */

namespace Cip
{
  /// <summary>
  /// List of CIP data segment's types
  /// </summary>
  public enum CipDataType
  {
    Simple = 0x00,
    Ansi = 0x11
  }
}
