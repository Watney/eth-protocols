﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         12-02-2020
 *  
 *  TITLE:        Cip Data Segment object
 *  DESCRIPTION:  Segment used to represent strings and generic data
 */

using UtilByte;
using System;
using System.IO;
using System.Text;

namespace Cip
{
  /// <summary>
  /// Segment used to represent strings and generic data
  /// </summary>
  public class CipDataSegment : CipSegment
  {
    #region ATTRIBUTES

    /// <summary>
    /// Data value to serialize
    /// </summary>
    public byte[] Data { get; private set; }

    /// <summary>
    /// Data segment type
    /// </summary>
    CipDataType type;

    #endregion

    #region CONSTRUCTORS

    public CipDataSegment(byte[] data) : base(CipSegmentID.Data)
    {
      type = CipDataType.Simple;
      Data = data;
    }

    public CipDataSegment(string data) : base(CipSegmentID.Data)
    {
      type = CipDataType.Ansi;
      Data = Encoding.UTF8.GetBytes(data);
    }

    public CipDataSegment(MemoryStream stream) : base(CipSegmentID.Data)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Data Segment in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      #region SERIALIZE SEGMENT ID

      int segId = ((int)Id) << 5;
      segId |= (int)type;

      writer.Write((byte)segId);

      #endregion

      if (type == CipDataType.Ansi)
        writer.Write((byte)Data.Length);
      else
        writer.Write((byte)(Data.Length / 2));

      writer.Write(Data);

      if (Data.Length % 2 != 0) writer.Write((byte)0);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Data Segment
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      #region DESERIALIZE SEGMENT ID

      byte segId = reader.ReadByte();

      Id = (CipSegmentID)(segId >> 5);      //  [000]  00000

      if (Id != CipSegmentID.Logical)
        throw new InvalidCastException("Invalid segment identifier for a Data Segment");

      type = (CipDataType)(segId & 0x1F);   //   000  [00000]

      #endregion

      byte length = reader.ReadByte();

      if (type == CipDataType.Ansi)
        Data = reader.ReadBytes(length);
      else
        Data = reader.ReadBytes(length * 2);

      if (length % 2 != 0) reader.ReadByte();
    }

    #endregion
  }
}
