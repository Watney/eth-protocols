﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cip Port Segment object
 *  DESCRIPTION:  Segment used to represents ports
 */

using UtilByte;
using System;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Segment used to represents ports
  /// </summary>
  public class CipPortSegment : CipSegment
  {
    #region ATTRIBUTES

    /// <summary>
    /// Port identifier
    /// </summary>
    public ushort PortId { get; set; }

    /// <summary>
    /// Link address 
    /// </summary>
    public byte[] LinkAddress { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipPortSegment(ushort portId) : this(portId, new byte[0])
    {

    }

    public CipPortSegment(ushort portId, byte[] linkAddress) : base(CipSegmentID.Port)
    {
      PortId = portId;
      LinkAddress = linkAddress;
    }

    public CipPortSegment(MemoryStream stream) : base(CipSegmentID.Port)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Port Segment in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      #region SERIALIZE SEGMENT ID

      bool extendedPort = PortId > 0xE;

      int segId = extendedPort ? 0xF : PortId;
      if (LinkAddress.Length > 0) segId |= (1 << 4);

      writer.Write((byte)segId);

      #endregion

      if (LinkAddress.Length > 0) writer.Write((byte)LinkAddress.Length);

      if (extendedPort) writer.Write(PortId);

      writer.Write(LinkAddress);

      if (writer.Index % 2 != 0) writer.Write((byte)0);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Port Segment
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      #region DESERIALIZE SEGMENT ID

      byte segId = reader.ReadByte();

      CipSegmentID id = (CipSegmentID)(segId >> 5);           // [000]  0   0000

      if (id != CipSegmentID.Port)
        throw new InvalidCastException("Invalid segment identifier for a Port Segment");

      bool extendedLinkAddress = ((segId & 0x10) >> 4) != 0;  //  000  [0]  0000

      PortId = (ushort)(segId & 0xF);                         //  000   0  [0000]

      #endregion

      if (extendedLinkAddress)
        LinkAddress = new byte[reader.ReadByte()];

      if (PortId == 0xF) //  Extended Port Id
        PortId = reader.ReadUInt16();

      LinkAddress = reader.ReadBytes(LinkAddress.Length);

      if (reader.Index % 2 != 0) reader.ReadByte();
    }

    #endregion
  }
}
