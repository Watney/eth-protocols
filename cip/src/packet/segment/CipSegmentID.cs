﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         20-01-2020
 *  
 *  TITLE:        Cip Segment ID enum
 *  DESCRIPTION:  List of CIP segment identifiers
 */

namespace Cip
{
  /// <summary>
  /// List of CIP segment identifiers
  /// </summary>
  public enum CipSegmentID : byte
  {
    Port = 0x0,
    Logical = 0x1,
    Data = 0x4
  }
}
