﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cip Segment Path object
 *  DESCRIPTION:  Object that include a list of Cip segments
 */

using UtilByte;
using System;
using System.IO;
using System.Collections.Generic;

namespace Cip
{
  /// <summary>
  /// Object that include a list of Cip segments
  /// </summary>
  public class CipSegmentPath : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// List of segments contained in the path
    /// </summary>
    public CipSegment[] Segments { get; private set; }

    #endregion

    #region CONSTRUCTORS

    public CipSegmentPath(params CipSegment[] segments)
    {
      Segments = segments;
    }

    public CipSegmentPath(MemoryStream stream)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialzie a Segment Path in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((byte)0);

      foreach (CipSegment seg in Segments)
        writer.Write(seg.Serialize());

      #region WRITE THE LENGTH OF SEGMENTS WRITTEN

      long end = writer.Index;
      writer.Index = 0;
      writer.Write((byte)((end - 1) / 2));
      writer.Index = end;

      #endregion

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Segment Path
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      ushort length = (ushort)(reader.ReadByte() * 2);

      Stack<CipSegment> segs = new Stack<CipSegment>();

      while (length > 0)
      {
        CipSegmentID segId = (CipSegmentID)(reader.ReadByte() >> 5);
        reader.Index--;

        switch (segId)
        {
          case CipSegmentID.Port: segs.Push(new CipPortSegment(stream)); break;
          case CipSegmentID.Logical: segs.Push(new CipLogicalSegment(stream)); break;
          case CipSegmentID.Data: segs.Push(new CipDataSegment(stream)); break;
          default: throw new InvalidDataException("Invalid Segment Identifier");
        }

        length -= (ushort)segs.Peek().Serialize().Length;
      }

      Segments = segs.ToArray();
    }

    #endregion
  }
}
