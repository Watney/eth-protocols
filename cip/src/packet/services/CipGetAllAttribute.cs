﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         09-03-2020
 *  
 *  TITLE:        Cip Get All Attribute object
 *  DESCRIPTION:  Service used to retreive all attributes' data
 */

using System.IO;

namespace Cip
{
  /// <summary>
  /// Service used to retreive all attributes' data
  /// </summary>
  public class CipGetAllAttribute : CipService
  {
    #region ATTRIBUTES

    /// <summary>
    /// Response data
    /// </summary>
    public byte[] ResponseData { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipGetAllAttribute(uint classId, uint instanceId) : base((byte)CipServiceID.GetAttributeAll, null, true)
    {
      ResponseData = null;

      RequestPath = new CipSegmentPath(
        new CipLogicalSegment(classId, CipLogicalType.Class),
        new CipLogicalSegment(instanceId, CipLogicalType.Instance));
    }

    public CipGetAllAttribute(string tag) : base((byte)CipServiceID.GetAttributeAll, null, true)
    {
      ResponseData = null;
      RequestPath = CipDataManager.GetRequestPath(tag);
    }

    public CipGetAllAttribute(byte[] respData) : base((byte)CipServiceID.GetAttributeAll, null, false)
    {
      ResponseData = respData;
    }

    public CipGetAllAttribute(MemoryStream stream, int length = 0) : base(stream, length)
    {

    }

    #endregion

    #region IMPLEMENT INTERFACE

    protected override byte[] SerializeRequest()
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(Id, RequestPath);
      return request.Serialize();
    }

    protected override byte[] SerializeResponse()
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(Id, ResponseData, CipStatus.SUCCESS);
      return response.Serialize();
    }

    protected override void DeserializeRequest(MemoryStream stream)
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(stream);
      RequestPath = request.Path;
    }

    protected override void DeserializeResponse(MemoryStream stream)
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(stream);

      if (response.Status != CipStatus.SUCCESS)
        throw new CipException(response.Status, response.AdditionalStatus);

      ResponseData = response.Data;
    }

    #endregion
  }
}
