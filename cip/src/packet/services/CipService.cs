﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         12-02-2020
 *  
 *  TITLE:        Cip Service object
 *  DESCRIPTION:  Generic object used to serialize cip services
 */

using UtilByte;
using System.IO;
using System;

namespace Cip
{
  /// <summary>
  /// Generic object used to serialize cip services
  /// </summary>
  public abstract class CipService
  {
    /// <summary>
    /// Connection manager object used in message router requests
    /// </summary>
    protected static CipSegmentPath ConnectionManager = new CipSegmentPath(
        new CipLogicalSegment(0x06, CipLogicalType.Class),
        new CipLogicalSegment(0x01, CipLogicalType.Instance));

    /// <summary>
    /// Message router object used in message router requests
    /// </summary>
    protected static CipSegmentPath MessageRouter = new CipSegmentPath(
        new CipLogicalSegment(0x02, CipLogicalType.Class),
        new CipLogicalSegment(0x01, CipLogicalType.Instance));

    #region ATTRIBUTES

    /// <summary>
    /// Identifier code of the service
    /// </summary>
    public byte Id { get; set; }

    /// <summary>
    /// Request path object used in message router requests
    /// </summary>
    public CipSegmentPath RequestPath { get; set; }

    /// <summary>
    /// Flag that indicates if the service is requested or responsed
    /// </summary>
    public bool IsRequest { get; private set; }

    #endregion

    #region CONSTRUCTORS

    public CipService(byte id, CipSegmentPath reqPath, bool isRequest = true)
    {
      Id = id;
      RequestPath = reqPath;
      IsRequest = isRequest;
    }

    public CipService(MemoryStream stream, int length = 0)
    {
      Deserialize(stream, length);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Cip Service in a byte stream 
    /// </summary>
    public byte[] Serialize()
    {
      return IsRequest ? SerializeRequest() : SerializeResponse();
    }

    protected abstract byte[] SerializeRequest();
    protected abstract byte[] SerializeResponse();

    /// <summary>
    /// Deserialize a byte stream in the Cip Service
    /// </summary>
    public void Deserialize(MemoryStream stream, int length = 0)
    {
      ByteReader reader = new ByteReader(stream);

      byte id = reader.ReadByte();
      reader.Index--;

      IsRequest = ((id >> 7) == 0);
      Id = (byte)(id & 0x7F);

      if (IsRequest)
        DeserializeRequest((length > 0) ? new MemoryStream(reader.ReadBytes(length)) : stream);
      else
        DeserializeResponse((length > 0) ? new MemoryStream(reader.ReadBytes(length)) : stream);
    }

    protected abstract void DeserializeRequest(MemoryStream stream);
    protected abstract void DeserializeResponse(MemoryStream stream);

    #endregion
  }
}
