﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         10-02-2020
 *  
 *  TITLE:        Cip Service enum
 *  DESCRIPTION:  Specifies the CIP Service code
 */

namespace Cip
{
  /// <summary>
  /// Specifies the CIP Service code
  /// </summary>
  public enum CipServiceID
  {
    GetAttributeAll = 0x01,
    SetAttributeAll = 0x02,
    GetAttributeList = 0x03,
    SetAttributeList = 0x04,
    GetAttributeSingle = 0x0E,
    MultipleServicePacket = 0x0A,
    ReadTag = 0x4C,
    WriteTag = 0x4D,
    ForwardClose = 0x4E,
    ForwardOpen = 0x54,
    LargeForwardOpen = 0x5B
  }
}
