﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         12-02-2020
 *  
 *  TITLE:        Cip Forward Close object
 *  DESCRIPTION:  Service used to close a connected communication
 */

using System.IO;

namespace Cip
{
  /// <summary>
  /// Service used to close a connected communication
  /// </summary>
  public class CipForwardClose : CipService
  {
    #region ATTRIBUTES

    /// <summary>
    /// Request close data sent from target to originator
    /// </summary>
    public CipForwardCloseRequest Request { get; set; }

    /// <summary>
    /// Response close data sent from originator to target
    /// </summary>
    public CipForwardCloseResponse Response { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipForwardClose(CipForwardCloseRequest request) : base((byte)CipServiceID.ForwardClose, CipService.ConnectionManager, true)
    {
      Request = request;
      Response = null;
    }

    public CipForwardClose(CipForwardCloseResponse response) : base((byte)CipServiceID.ForwardClose, CipService.ConnectionManager, false)
    {
      Request = null;
      Response = response;
    }

    public CipForwardClose(MemoryStream stream, int length = 0) : base(stream, length)
    {

    }

    #endregion

    #region IMPLEMENT INTERFACE

    protected override byte[] SerializeRequest()
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(Id, RequestPath, Request.Serialize());
      return request.Serialize();
    }

    protected override byte[] SerializeResponse()
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(Id, Response.Serialize(), CipStatus.SUCCESS);
      return response.Serialize();
    }

    protected override void DeserializeRequest(MemoryStream stream)
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(stream);
      RequestPath = request.Path;
      Request = new CipForwardCloseRequest(new MemoryStream(request.Data));
    }

    protected override void DeserializeResponse(MemoryStream stream)
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(stream);

      if (response.Status != CipStatus.SUCCESS)
        throw new CipException(response.Status, response.AdditionalStatus);

      Response = new CipForwardCloseResponse(new MemoryStream(response.Data));
    }

    #endregion
  }
}
