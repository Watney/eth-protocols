﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         13-02-2020
 *  
 *  TITLE:        Cip Get Single Attribute object
 *  DESCRIPTION:  Service used to retreive a single attribute data
 */

using System.IO;

namespace Cip
{
  /// <summary>
  /// Service used to retreive a single attribute data
  /// </summary>
  public class CipGetSingleAttribute : CipService
  {
    #region ATTRIBUTES

    /// <summary>
    /// Response data
    /// </summary>
    public byte[] ResponseData { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipGetSingleAttribute(uint classId, uint instanceId, uint attributeId) : base((byte)CipServiceID.GetAttributeSingle, null, true)
    {
      ResponseData = null;

      RequestPath = new CipSegmentPath(
        new CipLogicalSegment(classId, CipLogicalType.Class),
        new CipLogicalSegment(instanceId, CipLogicalType.Instance),
        new CipLogicalSegment(attributeId, CipLogicalType.Attribute));
    }

    public CipGetSingleAttribute(byte[] respData) : base((byte)CipServiceID.GetAttributeSingle, null, false)
    {
      ResponseData = respData;
    }

    public CipGetSingleAttribute(MemoryStream stream, int length = 0) : base(stream, length)
    {

    }

    #endregion

    #region IMPLEMENT INTERFACE

    protected override byte[] SerializeRequest()
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(Id, RequestPath);
      return request.Serialize();
    }

    protected override byte[] SerializeResponse()
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(Id, ResponseData, CipStatus.SUCCESS);
      return response.Serialize();
    }

    protected override void DeserializeRequest(MemoryStream stream)
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(stream);
      RequestPath = request.Path;
    }

    protected override void DeserializeResponse(MemoryStream stream)
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(stream);

      if (response.Status != CipStatus.SUCCESS)
        throw new CipException(response.Status, response.AdditionalStatus);

      ResponseData = response.Data;
    }

    #endregion
  }
}
