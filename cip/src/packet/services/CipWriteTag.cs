﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         14-02-2020
 *  
 *  TITLE:        Cip Write Tag object
 *  DESCRIPTION:  Service used to write data in a tag variable stored in the PLC
 */

using System.IO;

namespace Cip
{
  /// <summary>
  /// Service used to write data in a tag variable stored in the PLC
  /// </summary>
  public class CipWriteTag : CipService 
  {
    #region ATTRIBUTES

    /// <summary>
    /// Tag name of the variable requested
    /// </summary>
    public string RequestTag { get; set; }

    /// <summary>
    /// Requested items count (DEFAULT 1)
    /// </summary>
    public ushort RequestCount { get; set; }

    /// <summary>
    /// Requested data value
    /// </summary>
    public byte[] RequestData { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipWriteTag(string tag, byte[] value, ushort count = 1) : base((byte)CipServiceID.WriteTag, CipDataManager.GetRequestPath(tag))
    {
      RequestTag = tag;
      RequestCount = count;
      RequestData = value;
    }

    public CipWriteTag() : base((byte)CipServiceID.WriteTag, null, false)
    {

    }

    public CipWriteTag(MemoryStream stream, int length = 0) : base(stream, length)
    {

    }

    #endregion

    #region IMPLEMENT INTERFACE

    protected override byte[] SerializeRequest()
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(Id, RequestPath, RequestData);
      return request.Serialize();
    }

    protected override byte[] SerializeResponse()
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(Id, new byte[0], CipStatus.SUCCESS);
      return response.Serialize();
    }

    protected override void DeserializeRequest(MemoryStream stream)
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(stream);
      RequestPath = request.Path;

      RequestTag = CipDataManager.GetTag(RequestPath);
      RequestData = request.Data;
    }

    protected override void DeserializeResponse(MemoryStream stream)
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(stream);

      if (response.Status != CipStatus.SUCCESS)
        throw new CipException(response.Status, response.AdditionalStatus);
    }

    #endregion
  }
}
