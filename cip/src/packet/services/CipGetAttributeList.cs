﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         12-02-2020
 *  
 *  TITLE:        Cip Get Attribute List object
 *  DESCRIPTION:  Service used to retreive a list of attributes
 */

using UtilByte;
using System;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Service used to retreive a list of attributes
  /// </summary>
  public class CipGetAttributeList : CipService
  {
    #region ATTRIBUTES

    /// <summary>
    /// List of attributes' ids requested
    /// </summary>
    public ushort[] AttributeIDs { get; set; }

    /// <summary>
    /// List of attributes' sizes requested
    /// </summary>
    public ushort[] AttributeSizes { get; set; }

    /// <summary>
    /// List of attributes' data responsed
    /// </summary>
    public CipAttributeResponse[] AttributeResponsed { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipGetAttributeList(CipSegmentPath reqPath, ushort[] attrIds, ushort[] attrSizes) : base((byte)CipServiceID.GetAttributeList, reqPath, true)
    {
      AttributeIDs = attrIds;
      AttributeSizes = attrSizes;
      AttributeResponsed = null;
    }

    public CipGetAttributeList(params CipAttributeResponse[] attrResponsed) : base((byte)CipServiceID.GetAttributeList, null, false)
    {
      AttributeIDs = null;
      AttributeSizes = null;
      AttributeResponsed = attrResponsed;
    }

    public CipGetAttributeList(MemoryStream stream, int length = 0) : base(stream, length)
    {

    }

    #endregion

    #region IMPLEMENT INTERFACE

    protected override byte[] SerializeRequest()
    {
      #region SERIALIZE REQUEST DATA

      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((ushort)AttributeIDs.Length);

      foreach (ushort id in AttributeIDs)
        writer.Write(id);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] data = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), data, data.Length);

      #endregion

      #endregion

      CipMessageRouterRequest request = new CipMessageRouterRequest(Id, RequestPath, data);
      return request.Serialize();
    }

    protected override byte[] SerializeResponse()
    {
      #region SERIALIZE RESPONSE DATA

      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((ushort)AttributeResponsed.Length);

      foreach (CipAttributeResponse attr in AttributeResponsed)
        writer.Write(attr.Serialize());

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] data = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), data, data.Length);

      #endregion

      #endregion

      CipMessageRouterResponse response = new CipMessageRouterResponse(Id, data, CipStatus.SUCCESS);
      return response.Serialize();
    }

    protected override void DeserializeRequest(MemoryStream stream)
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(stream);
      RequestPath = request.Path;

      #region DESERIALIZE REQUEST DATA

      ByteReader reader = new ByteReader(new MemoryStream(request.Data));

      AttributeIDs = new ushort[reader.ReadUInt16()];

      for (int i = 0; i < AttributeIDs.Length; i++)
        AttributeIDs[i] = reader.ReadUInt16();

      #endregion
    }

    protected override void DeserializeResponse(MemoryStream stream)
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(stream);

      if (response.Status != CipStatus.SUCCESS)
        throw new CipException(response.Status, response.AdditionalStatus);

      #region DESERIALIZE RESPONSE DATA

      ByteReader reader = new ByteReader(new MemoryStream(response.Data));

      AttributeResponsed = new CipAttributeResponse[reader.ReadUInt16()];

      for (int i = 0; i < AttributeResponsed.Length; i++)
        AttributeResponsed[i] = new CipAttributeResponse(stream, AttributeSizes[i]);

      #endregion
    }

    #endregion
  }
}
