﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         14-02-2020
 *  
 *  TITLE:        Cip Read Tag object
 *  DESCRIPTION:  Service used to read data from a tag variable stored in the PLC
 */

using System;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Service used to read data from a tag variable stored in the PLC
  /// </summary>
  public class CipReadTag : CipService
  {
    #region ATTRIBUTES

    /// <summary>
    /// Tag name of the variable requested
    /// </summary>
    public string RequestTag { get; set; }

    /// <summary>
    /// Requested items count (DEFAULT 1)
    /// </summary>
    public ushort RequestCount { get; set; }

    /// <summary>
    /// Requested items length (if tag requetsed is array) 
    /// </summary>
    public int RequestLength { get; set; }

    /// <summary>
    /// Responsed data value
    /// </summary>
    public byte[] ResponseData { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipReadTag(string tag, ushort count = 1) : base((byte)CipServiceID.ReadTag, CipDataManager.GetRequestPath(tag))
    {
      RequestTag = tag;
      RequestCount = count;
    }

    public CipReadTag(byte[] response) : base((byte)CipServiceID.ReadTag, null, false)
    {
      ResponseData = response;
    }

    public CipReadTag(MemoryStream stream, int length = 0) : base(stream, length)
    {

    }

    #endregion

    #region IMPLEMENT INTERFACE

    protected override byte[] SerializeRequest()
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(Id, RequestPath, BitConverter.GetBytes(RequestCount));
      return request.Serialize();
    }

    protected override byte[] SerializeResponse()
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(Id, ResponseData, CipStatus.SUCCESS);
      return response.Serialize();
    }

    protected override void DeserializeRequest(MemoryStream stream)
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(stream);
      RequestPath = request.Path;

      RequestTag = CipDataManager.GetTag(RequestPath);
      RequestCount = BitConverter.ToUInt16(request.Data, 0);
    }

    protected override void DeserializeResponse(MemoryStream stream)
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(stream);

      if (response.Status != CipStatus.SUCCESS)
        throw new CipException(response.Status, response.AdditionalStatus);

      ResponseData = response.Data;
    }

    #endregion
  }
}
