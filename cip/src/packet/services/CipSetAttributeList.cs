﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         12-02-2020
 *  
 *  TITLE:        Cip Set Attribute List object
 *  DESCRIPTION:  Service used to modify a list of attributes
 */

using UtilByte;
using System;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Service used to modify a list of attributes
  /// </summary>
  public class CipSetAttributeList : CipService
  {
    #region ATTRIBUTES

    /// <summary>
    /// List of attributes requested
    /// </summary>
    public CipAttributeRequest[] AttributeRequested { get; set; }

    /// <summary>
    /// List of attributes responsed
    /// </summary>
    public CipAttributeResponse[] AttributeResponsed { get; set; }

    #endregion

    #region CONSTRUCTORS 

    public CipSetAttributeList(CipSegmentPath reqPath, params CipAttributeRequest[] attrRequested) : base((byte)CipServiceID.SetAttributeList, reqPath, true)
    {
      AttributeRequested = attrRequested;
      AttributeResponsed = null;
    }

    public CipSetAttributeList(params CipAttributeResponse[] attrResponsed) : base((byte)CipServiceID.SetAttributeList, null, true)
    {
      AttributeRequested = null;
      AttributeResponsed = attrResponsed;
    }

    public CipSetAttributeList(MemoryStream stream, int length = 0) : base(stream, length)
    {

    }

    #endregion

    #region IMPLEMENT INTERFACE

    protected override byte[] SerializeRequest()
    {
      #region SERIALIZE REQUEST DATA

      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((ushort)AttributeRequested.Length);

      foreach (CipAttributeRequest attr in AttributeRequested)
        writer.Write(attr.Serialize());

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] data = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), data, data.Length);

      #endregion

      #endregion

      CipMessageRouterRequest request = new CipMessageRouterRequest(Id, RequestPath, data);
      return request.Serialize();
    }

    protected override byte[] SerializeResponse()
    {
      #region SERIALIZE RESPONSE DATA

      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((ushort)AttributeResponsed.Length);

      foreach (CipAttributeResponse attr in AttributeResponsed)
        writer.Write(attr.Serialize());

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] data = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), data, data.Length);

      #endregion

      #endregion

      CipMessageRouterResponse response = new CipMessageRouterResponse(Id, data, CipStatus.SUCCESS);
      return response.Serialize();
    }

    protected override void DeserializeRequest(MemoryStream stream)
    {
      throw new NotImplementedException();
    }

    protected override void DeserializeResponse(MemoryStream stream)
    {
      throw new NotImplementedException();
    }

    #endregion
  }
}
