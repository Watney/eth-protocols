﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         12-02-2020
 *  
 *  TITLE:        Cip Forward Open object
 *  DESCRIPTION:  Service used to open a connected communication
 */

using System.IO;

namespace Cip
{
  /// <summary>
  /// Service used to open a connected communication
  /// </summary>
  public class CipForwardOpen : CipService
  {
    #region ATTRIBUTES

    /// <summary>
    /// Request open data sent from target to originator
    /// </summary>
    public CipForwardOpenRequest Request { get; set; }

    /// <summary>
    /// Response open data sent from originator to target
    /// </summary>
    public CipForwardOpenResponse Response { get; set; }

    /// <summary>
    /// Flag that indicates if the request is large or normal
    /// </summary>
    public bool IsLargeRequest { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CipForwardOpen(CipForwardOpenRequest request) : 
      base(request.IsLarge ? (byte)CipServiceID.LargeForwardOpen : (byte)CipServiceID.ForwardOpen, CipService.ConnectionManager, true)
    {
      Request = request;
      Response = null;
      IsLargeRequest = request.IsLarge;
    }

    public CipForwardOpen(CipForwardOpenResponse response) : base((byte)CipServiceID.ForwardOpen, CipService.ConnectionManager, false)
    {
      Request = null;
      Response = response;
      IsLargeRequest = false;
    }

    public CipForwardOpen(MemoryStream stream, int length = 0) : base(stream, length)
    {
    }

    #endregion

    #region IMPLEMENT INTERFACE

    protected override byte[] SerializeRequest()
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(Id, RequestPath, Request.Serialize());
      return request.Serialize();
    }

    protected override byte[] SerializeResponse()
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(Id, Response.Serialize(), CipStatus.SUCCESS);
      return response.Serialize();
    }

    protected override void DeserializeRequest(MemoryStream stream)
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(stream);
      RequestPath = request.Path;
      IsLargeRequest = (CipServiceID)request.Service == CipServiceID.LargeForwardOpen;
      Request = new CipForwardOpenRequest(new MemoryStream(request.Data), IsLargeRequest);
    }

    protected override void DeserializeResponse(MemoryStream stream)
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(stream);

      if (response.Status != CipStatus.SUCCESS)
        throw new CipException(response.Status, response.AdditionalStatus);

      Response = new CipForwardOpenResponse(new MemoryStream(response.Data));
    }

    #endregion
  }
}
