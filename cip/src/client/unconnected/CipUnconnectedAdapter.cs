﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         17-03-2020
 *  
 *  TITLE:        Cip Unconnected Adapter object
 *  DESCRIPTION:  Synchronous Unconnected Cip Adapter
 */

using Enip;
using System.IO;

namespace Cip
{
  /// <summary>
  /// Synchronous Unconnected Cip Adapter
  /// </summary>
  public class CipUnconnectedAdapter : EnipClient
  {
    public const uint MAX_RESPONSE_SIZE = 490;
    public const uint MAX_REQUEST_SIZE = 440;

    /// <summary>
    /// Default connection path for unconnected communications 
    /// </summary>
    private static CipSegmentPath ConnectionPath = new CipSegmentPath(new CipPortSegment(1));

    #region CONSTRUCTORS

    public CipUnconnectedAdapter(string ip) : base(ip)
    {

    }

    public CipUnconnectedAdapter(string ip, ushort port) : base(ip, port)
    {

    }

    #endregion

    #region FUNCTIONS TO READ & WRITE TAGS

    /// <summary>
    /// Function used to retreive a PLC tag of ARRAY type
    /// </summary>
    public T ReadTag<T>(string variable, int length, ushort count = 1)
    {
      CipReadTag service = new CipReadTag(variable, count);
      byte[] response = SendUnconnected(service);

      service = new CipReadTag(new MemoryStream(response));
      return CipDataManager.Deserialize<T>(new MemoryStream(service.ResponseData), length, count, false);
    }

    /// <summary>
    /// Function used to retreive a PLC tag of ATOMIC or STRUCT type
    /// </summary>
    public T ReadTag<T>(string variable, ushort count = 1)
    {
      return ReadTag<T>(variable, 1, count);
    }

    /// <summary>
    /// Function used to write data in a PLC tag
    /// </summary>
    public void WriteTag<T>(string variable, T value)
    {
      CipWriteTag service = new CipWriteTag(variable, CipDataManager.Serialize<T>(value, 1, true));
      byte[] response = SendUnconnected(service);
    }

    #endregion

    /// <summary>
    /// Generic function used to send a Cip Service packet and retreive the response 
    /// </summary>
    protected byte[] SendUnconnected(CipService service)
    {
      byte[] result = null;

      CdfPacket packet = new CdfItem[]
      {
        new CdfNullAddress(),
        new CdfUnconnectedPacket(service.Serialize())
      };
      EnipSendRRData response = base.SendRRPacket(new EnipSendRRData(packet)).Command as EnipSendRRData;

      foreach (CdfItem item in response.Packet.Items)
      {
        if (item is CdfUnconnectedPacket)
        {
          result = (item as CdfUnconnectedPacket).Data;
          break;
        }
      }

      return result;
    }
  }
}
