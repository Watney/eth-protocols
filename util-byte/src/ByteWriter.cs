﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         15-03-2020
 *  
 *  TITLE:        Byte Writer
 *  DESCRIPTION:  Override of the Binary Writer obejct with the endian encoding option
 */

using System;
using System.IO;

namespace UtilByte
{
  /// <summary>
  /// Override of the Binary Writer obejct with the endian encoding option
  /// </summary>
  public class ByteWriter : BinaryWriter
  {
    #region ATTRIBUTES

    /// <summary>
    /// Reading index inside the source stream
    /// </summary>
    public long Index
    {
      get => BaseStream.Position;
      set => BaseStream.Position = value;
    }

    /// <summary>
    /// Flag that indicates the way to read bytes from the source stream
    /// </summary>
    public bool IsLittleEndian { get; set; }

    #endregion

    #region CONSTRUCTORS

    public ByteWriter(MemoryStream source, bool isLittleEndian = true) : base(source)
    {
      IsLittleEndian = isLittleEndian;
    }

    #endregion

    #region OVERRIDE OF WRITING FUNCTIONS

    /// <summary>
    /// Writes a 4-bytes floating-point value to the current stream and advances the stream position by 4 bytes
    /// </summary>
    public override void Write(float value)
    {
      byte[] bytes = BitConverter.GetBytes(value);

      if (!IsLittleEndian)
        Array.Reverse(bytes);

      base.Write(BitConverter.ToSingle(bytes, 0));
    }

    /// <summary>
    /// Writes an 8-bytes floating-point value to the current stream and advances the stream position by 8 bytes
    /// </summary>
    public override void Write(double value)
    {
      byte[] bytes = BitConverter.GetBytes(value);

      if (!IsLittleEndian)
        Array.Reverse(bytes);

      base.Write(BitConverter.ToDouble(bytes, 0));
    }


    /// <summary>
    /// Writes a 2-bytes signed integer value to the current stream and advances the stream position by 2 bytes
    /// </summary>
    public override void Write(short value)
    {
      byte[] bytes = BitConverter.GetBytes(value);

      if (!IsLittleEndian)
        Array.Reverse(bytes);

      base.Write(BitConverter.ToInt16(bytes, 0));
    }

    /// <summary>
    /// Writes a 4-bytes signed integer value to the current stream and advances the stream position by 4 bytes
    /// </summary>
    public override void Write(int value)
    {
      byte[] bytes = BitConverter.GetBytes(value);

      if (!IsLittleEndian)
        Array.Reverse(bytes);

      base.Write(BitConverter.ToInt32(bytes, 0));
    }

    /// <summary>
    /// Writes a 8-bytes signed integer value to the current stream and advances the stream position by 8 bytes
    /// </summary>
    public override void Write(long value)
    {
      byte[] bytes = BitConverter.GetBytes(value);

      if (!IsLittleEndian)
        Array.Reverse(bytes);

      base.Write(BitConverter.ToInt64(bytes, 0));
    }


    /// <summary>
    /// Writes a 2-bytes unsigned integer value to the current stream and advances the stream position by 2 bytes
    /// </summary>
    public override void Write(ushort value)
    {
      byte[] bytes = BitConverter.GetBytes(value);

      if (!IsLittleEndian)
        Array.Reverse(bytes);

      base.Write(BitConverter.ToUInt16(bytes, 0));
    }

    /// <summary>
    /// Writes a 4-bytes unsigned integer value to the current stream and advances the stream position by 4 bytes
    /// </summary>
    public override void Write(uint value)
    {
      byte[] bytes = BitConverter.GetBytes(value);

      if (!IsLittleEndian)
        Array.Reverse(bytes);

      base.Write(BitConverter.ToUInt32(bytes, 0));
    }

    /// <summary>
    /// Writes a 8-bytes unsigned integer value to the current stream and advances the stream position by 8 bytes
    /// </summary>
    public override void Write(ulong value)
    {
      byte[] bytes = BitConverter.GetBytes(value);

      if (!IsLittleEndian)
        Array.Reverse(bytes);

      base.Write(BitConverter.ToUInt64(bytes, 0));
    }


    /// <summary>
    /// Writes a Boolean value to the current stream and advances the stream position by 2 bytes
    /// </summary>
    public override void Write(bool value)
    {
      this.Write(value ? (ushort)1 : (ushort)0);
    }

    #endregion
  }
}
