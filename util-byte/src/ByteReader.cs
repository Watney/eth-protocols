﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         15-03-2020
 *  
 *  TITLE:        Byte Reader
 *  DESCRIPTION:  Override of the Binary Reader obejct with the endian encoding option
 */

using System;
using System.IO;

namespace UtilByte
{
  /// <summary>
  /// Override of the Binary Reader obejct with the endian encoding option
  /// </summary>
  public class ByteReader : BinaryReader
  {
    #region ATTRIBUTES

    /// <summary>
    /// Reading index inside the source stream
    /// </summary>
    public long Index
    {
      get => BaseStream.Position;
      set => BaseStream.Position = value;
    }

    /// <summary>
    /// Flag that indicates the way to read bytes from the source stream
    /// </summary>
    public bool IsLittleEndian { get; set; }

    #endregion

    #region CONSTRUCTORS

    public ByteReader(MemoryStream source, bool isLittleEndian = false) : base(source)
    {
      IsLittleEndian = isLittleEndian;
    }

    #endregion

    #region OVERRIDE OF READING FUNCTIONS

    /// <summary>
    /// Reads an 4-byte floating point value from current stream and advances the current position of the stream by 4 bytes
    /// </summary>
    public override float ReadSingle()
    {
      byte[] bytes = BitConverter.GetBytes(base.ReadSingle());

      if(IsLittleEndian)
        Array.Reverse(bytes);

      return BitConverter.ToSingle(bytes, 0);
    }

    /// <summary>
    /// Reads an 8-byte floating point value from current stream and advances the current position of the stream by 8 bytes
    /// </summary>
    public override double ReadDouble()
    {
      byte[] bytes = BitConverter.GetBytes(base.ReadDouble());

      if (IsLittleEndian)
        Array.Reverse(bytes);

      return BitConverter.ToDouble(bytes, 0);
    }


    /// <summary>
    /// Reads a 2-byte signed integer from current stream and advances the current position of the stream by 2 bytes
    /// </summary>
    public override short ReadInt16()
    {
      byte[] bytes = BitConverter.GetBytes(base.ReadInt16());

      if (IsLittleEndian)
        Array.Reverse(bytes);

      return BitConverter.ToInt16(bytes, 0);
    }

    /// <summary>
    /// Reads a 4-byte signed integer from current stream and advances the current position of the stream by 4 bytes
    /// </summary>
    public override int ReadInt32()
    {
      byte[] bytes = BitConverter.GetBytes(base.ReadInt32());

      if (IsLittleEndian)
        Array.Reverse(bytes);

      return BitConverter.ToInt32(bytes, 0);
    }

    /// <summary>
    /// Reads a 8-byte signed integer from current stream and advances the current position of the stream by 8 bytes
    /// </summary>
    public override long ReadInt64()
    {
      byte[] bytes = BitConverter.GetBytes(base.ReadInt64());

      if (IsLittleEndian)
        Array.Reverse(bytes);

      return BitConverter.ToInt64(bytes, 0);
    }


    /// <summary>
    /// Reads a 2-byte unsigned integer from current stream and advances the current position of the stream by 2 bytes
    /// </summary>
    public override ushort ReadUInt16()
    {
      byte[] bytes = BitConverter.GetBytes(base.ReadUInt16());

      if (IsLittleEndian)
        Array.Reverse(bytes);

      return BitConverter.ToUInt16(bytes, 0);
    }

    /// <summary>
    /// Reads a 4-byte unsigned integer from current stream and advances the current position of the stream by 4 bytes
    /// </summary>
    public override uint ReadUInt32()
    {
      byte[] bytes = BitConverter.GetBytes(base.ReadUInt32());

      if (IsLittleEndian)
        Array.Reverse(bytes);

      return BitConverter.ToUInt32(bytes, 0);
    }

    /// <summary>
    /// Reads a 8-byte unsigned integer from current stream and advances the current position of the stream by 8 bytes
    /// </summary>
    public override ulong ReadUInt64()
    {
      byte[] bytes = BitConverter.GetBytes(base.ReadUInt64());

      if (IsLittleEndian)
        Array.Reverse(bytes);

      return BitConverter.ToUInt64(bytes, 0);
    }


    /// <summary>
    /// Reads a Boolean value from current stream and advances the current position of the stream by 2 bytes
    /// </summary>
    public override bool ReadBoolean()
    {
      return this.ReadUInt16() != 0;
    }

    #endregion
  }
}
