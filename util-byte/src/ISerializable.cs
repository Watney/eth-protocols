﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         16-01-2020
 *  
 *  TITLE:        ISerializable interface
 *  DESCRIPTION:  Interface used to define functions to convert objects in byte stream and viceversa
 */

using System.IO;

namespace UtilByte
{
  /// <summary>
  /// Interface used to define functions to convert objects in byte stream and viceversa
  /// </summary>
  public interface ISerializable
  {
    /// <summary>
    /// Convert the object in a byte stream
    /// </summary>
    byte[] Serialize();

    /// <summary>
    /// Obtain object data from a byte stream
    /// </summary>
    void Deserialize(MemoryStream stream);
  }
}
