﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cdf Connected Packet object
 *  DESCRIPTION:  Used to send request/response message in a connected communication using ethernet-ip protocol
 */

using UtilByte;
using System.IO;
using System;

namespace Enip
{
  /// <summary>
  /// Used to send request/response message in a connected communication using ethernet-ip protocol
  /// </summary>
  public class CdfConnectedPacket : CdfItem
  {
    #region ATTRIBUTES

    /// <summary>
    /// Generic data transmitted using a connected packet
    /// </summary>
    public byte[] Data { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CdfConnectedPacket(byte[] data) : base(CdfTypeID.CONNECTION_TRANSPORT)
    {
      Data = data;
    }

    public CdfConnectedPacket(MemoryStream stream) : base(CdfTypeID.CONNECTION_TRANSPORT)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Connected Packet object in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((ushort)Type);
      writer.Write((ushort)0);
      writer.Write(Data);

      #region WRITE THE SIZE OF THE PACKET

      long endPos = writer.Index;
      writer.Index = 2;
      writer.Write((ushort)endPos - 4);
      writer.Index = endPos;

      #endregion

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Connected Packet object
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Type = (CdfTypeID)reader.ReadUInt16();
      ushort dataLength = reader.ReadUInt16();
      Data = reader.ReadBytes(dataLength);
    }

    #endregion
  }
}
