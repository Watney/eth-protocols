﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cdf Socket Address OT object
 *  DESCRIPTION:  Provides socket informations about the output assembly
 */

using UtilByte;
using System;
using System.IO;

namespace Enip
{
  /// <summary>
  /// Provides socket informations about the output assembly
  /// </summary>
  public class CdfSocketAddressOT : CdfItem
  {
    #region ATTRIBUTES

    /// <summary>
    /// Socket informations of the device
    /// </summary>
    public SockAddr SocketAddress { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CdfSocketAddressOT() : base(CdfTypeID.SOCK_ADDR_INFO_OT)
    {

    }

    public CdfSocketAddressOT(MemoryStream stream) : base(CdfTypeID.SOCK_ADDR_INFO_OT)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Socket Address OT object in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((ushort)Type);
      writer.Write(SocketAddress.Serialize().Length);
      writer.Write(SocketAddress.Serialize());

      #region WRITE THE SIZE OF THE PACKET

      long endPos = writer.Index;
      writer.Index = 2;
      writer.Write((ushort)endPos - 4);
      writer.Index = endPos;

      #endregion

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Socket Address OT object
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Type = (CdfTypeID)reader.ReadUInt16();
      ushort length = reader.ReadUInt16();  // Length of the packet is READ-ONLY
      SocketAddress = new SockAddr(stream);
    }

    #endregion
  }
}
