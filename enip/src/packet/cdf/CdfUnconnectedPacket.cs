﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cdf Unconnected Packet object
 *  DESCRIPTION:  Used to send request/response message in a not connected communication using ethernet-ip protocol
 */

using UtilByte;
using System;
using System.IO;

namespace Enip
{
  /// <summary>
  /// Used to send request/response message in a not connected communication using ethernet-ip protocol
  /// </summary>
  public class CdfUnconnectedPacket : CdfItem
  {
    #region ATTRIBUTES

    /// <summary>
    /// Generic data transmitted using a not connected packet
    /// </summary>
    public byte[] Data { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CdfUnconnectedPacket(byte[] data) : base(CdfTypeID.UNCONNECTED_MSG)
    {
      Data = data;
    }

    public CdfUnconnectedPacket(MemoryStream stream) : base(CdfTypeID.UNCONNECTED_MSG)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Unconnected Packet object in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((ushort)Type);
      writer.Write((ushort)Data.Length);
      writer.Write(Data);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Unconnected Packet object
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Type = (CdfTypeID)reader.ReadUInt16();
      ushort dataLength = reader.ReadUInt16();
      Data = reader.ReadBytes(dataLength);
    }

    #endregion
  }
}
