﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         31-01-2020
 *  
 *  TITLE:        Cdf Null Address object
 *  DESCRIPTION:  Identifies an empty adress in not connected packeet using ethernet-ip protocol
 */

using UtilByte;
using System;
using System.IO;

namespace Enip
{
  /// <summary>
  /// Identifies an empty adress in not connected packeet using ethernet-ip protocol
  /// </summary>
  public class CdfNullAddress : CdfItem
  {
    #region CONSTRUCTOR

    public CdfNullAddress() : base(CdfTypeID.CDF_NULL)
    {

    }

    public CdfNullAddress(MemoryStream stream) : base(CdfTypeID.CDF_NULL)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Null Address object in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((ushort)Type);
      writer.Write((ushort)0);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Null Address object 
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Type = (CdfTypeID)reader.ReadUInt16();
      reader.ReadUInt16();
    }

    #endregion
  }
}
