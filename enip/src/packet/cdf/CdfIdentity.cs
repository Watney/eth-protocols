﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cdf Identity object
 *  DESCRIPTION:  Represents a device in an ethernet-ip communication
 */

using UtilByte;
using System;
using System.IO;
using System.Text;

namespace Enip
{
  /// <summary>
  /// Represents a device in an ethernet-ip communication
  /// </summary>
  public class CdfIdentity : CdfItem
  {
    #region ATTRIBUTES

    /// <summary>
    /// Version number of the ethernet-ip protocol used
    /// </summary>
    public ushort ProtocolVersion { get; set; }

    /// <summary>
    /// Socket informations of the device in the ethernet-ip communication
    /// </summary>
    public SockAddr SocketAddress { get; set; }

    /// <summary>
    /// Identifies the vendor that markets the device
    /// </summary>
    public ushort VendorID { get; set; }

    /// <summary>
    /// Specifies which profile has been used for this device
    /// </summary>
    public ushort DeviceType { get; set; }

    /// <summary>
    /// Code used to distinguish multiple products of the same vendor ID
    /// </summary>
    public ushort ProductCode { get; set; }

    /// <summary>
    /// Indicates the major revision of the device 
    /// </summary>
    public byte RevisionMajor { get; set; }

    /// <summary>
    /// Indicates the minor revision of the device 
    /// </summary>
    public byte RevisionMinor { get; set; }

    /// <summary>
    /// Provides information about the status of the device
    /// </summary>
    public ushort Status { get; set; }

    /// <summary>
    /// Used to uniquely identify individual devices
    /// </summary>
    public uint SerialNumber { get; set; }

    /// <summary>
    /// ASCII name string of the device
    /// </summary>
    public string ProductName { get; set; }

    /// <summary>
    /// Current state of the device
    /// </summary>
    public byte State { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CdfIdentity() : base(CdfTypeID.LIST_IDENTITY)
    {

    }

    public CdfIdentity(MemoryStream stream) : base(CdfTypeID.LIST_IDENTITY)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Identity object in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((ushort)Type);
      writer.Write((ushort)0);
      writer.Write(ProtocolVersion);
      writer.Write(SocketAddress.Serialize());
      writer.Write(VendorID);
      writer.Write(DeviceType);
      writer.Write(ProductCode);
      writer.Write(RevisionMajor);
      writer.Write(RevisionMinor);
      writer.Write(Status);
      writer.Write(SerialNumber);
      writer.Write((byte)ProductName.Length);
      writer.Write(Encoding.UTF8.GetBytes(ProductName));
      writer.Write(State);

      #region WRITE THE SIZE OF THE PACKET

      long endPos = writer.Index;
      writer.Index = 2;
      writer.Write((ushort)endPos - 4);
      writer.Index = endPos;

      #endregion

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Identity object
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Type = (CdfTypeID)reader.ReadUInt16();
      ushort length = reader.ReadUInt16();  //  Length of the packet is READ-ONLY
      ProtocolVersion = reader.ReadUInt16();
      SocketAddress = new SockAddr(stream);
      VendorID = reader.ReadUInt16();
      DeviceType = reader.ReadUInt16();
      ProductCode = reader.ReadUInt16();
      RevisionMajor = reader.ReadByte();
      RevisionMinor = reader.ReadByte();
      Status = reader.ReadUInt16();
      SerialNumber = reader.ReadUInt32();
      byte nameLength = reader.ReadByte();  //  Length of the name is READ-ONLY
      ProductName = Encoding.UTF8.GetString(reader.ReadBytes(nameLength));
      State = reader.ReadByte();
    }

    #endregion
  }
}
