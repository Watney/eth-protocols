﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cdf Connected Address object
 *  DESCRIPTION:  Used to send/retreive the connection identifier in a ethernet-ip communication
 */

using UtilByte;
using System;
using System.IO;

namespace Enip
{
  /// <summary>
  /// Used to send/retreive the connection identifier in a ethernet-ip communication
  /// </summary>
  public class CdfConnectedAddress : CdfItem
  {
    #region ATTRIBUTES

    /// <summary>
    /// Connection identifier, exchanged in the Forward Open service of the Connection Manager
    /// </summary>
    public uint ConnectionID { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CdfConnectedAddress() : base(CdfTypeID.CONNECTION_BASED)
    {

    }

    public CdfConnectedAddress(MemoryStream stream) : base(CdfTypeID.CONNECTION_BASED)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Connected Address object in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((ushort)Type);
      writer.Write((ushort)0);
      writer.Write(ConnectionID);

      #region WRITE THE SIZE OF THE PACKET

      long endPos = writer.Index;
      writer.Index = 2;
      writer.Write((ushort)endPos - 4);
      writer.Index = endPos;

      #endregion

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Connected Address object
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Type = (CdfTypeID)reader.ReadUInt16();
      ushort length = reader.ReadUInt16();  //Length of the packet is READ-ONLY
      ConnectionID = reader.ReadUInt32();
    }

    #endregion
  }
}
