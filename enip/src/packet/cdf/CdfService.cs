﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         18-02-2020
 *  
 *  TITLE:        Cdf Service object
 *  DESCRIPTION:  Represents a service in an ethernet-ip communication
 */

using UtilByte;
using System;
using System.IO;
using System.Text;

namespace Enip
{
  /// <summary>
  /// Represents a service in an ethernet-ip communication
  /// </summary>
  public class CdfService : CdfItem
  {
    #region ATTRIBUTES

    /// <summary>
    /// Version number of the ethernet-ip protocol used
    /// </summary>
    public ushort ProtocolVersion { get; set; }

    /// <summary>
    /// Flags that indicates which transport protocols are supported
    /// </summary>
    public CapabilityFlags CapabilityFlags { get; set; }

    /// <summary>
    /// Description name of the service
    /// </summary>
    public string Name { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CdfService() : base(CdfTypeID.LIST_SERVICE)
    {

    }

    public CdfService(MemoryStream stream) : base(CdfTypeID.LIST_SERVICE)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Service object in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((ushort)Type);
      writer.Write((ushort)0);
      writer.Write(ProtocolVersion);
      writer.Write((ushort)CapabilityFlags);

      #region SERIALIZE SERVICE NAME

      while (Name.Length < 16)
        Name += '\0';

      writer.Write(Encoding.UTF8.GetBytes(Name));

      #endregion

      #region WRITE THE SIZE OF THE PACKET

      long endPos = writer.Index;
      writer.Index = 2;
      writer.Write((ushort)endPos - 4);
      writer.Index = endPos;

      #endregion

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Service object
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Type = (CdfTypeID)reader.ReadUInt16();
      ushort length = reader.ReadUInt16();  //Length of the packet is READ-ONLY
      ProtocolVersion = reader.ReadUInt16();
      CapabilityFlags = (CapabilityFlags)reader.ReadUInt16();
      Name = Encoding.UTF8.GetString(reader.ReadBytes(16)).TrimEnd('\0');
    }

    #endregion
  }
}
