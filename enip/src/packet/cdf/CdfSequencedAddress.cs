﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cdf Sequenced Address object
 *  DESCRIPTION:  Used to send/retreive the connection identifier and the sequence number in a ethernet-ip communication
 */

using UtilByte;
using System;
using System.IO;

namespace Enip
{
  /// <summary>
  /// Used to send/retreive the connection identifier and the sequence number in a ethernet-ip communication
  /// </summary>
  public class CdfSequencedAddress : CdfItem
  {
    #region ATTRIBUTES

    /// <summary>
    /// Connection identifier, used in connected packet
    /// </summary>
    public uint ConnectionID { get; set; }

    /// <summary>
    /// Sequence number, used in connected packet
    /// </summary>
    public uint SequenceNumber { get; set; }

    #endregion

    #region CONSTRUCTORS

    public CdfSequencedAddress() : base(CdfTypeID.SEQ_ADDRESS)
    {

    }

    public CdfSequencedAddress(MemoryStream stream) : base(CdfTypeID.SEQ_ADDRESS)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Sequenced Address object in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((ushort)Type);
      writer.Write((ushort)0);
      writer.Write(ConnectionID);
      writer.Write(SequenceNumber);

      #region WRITE THE SIZE OF THE PACKET

      long endPos = writer.Index;
      writer.Index = 2;
      writer.Write((ushort)endPos - 4);
      writer.Index = endPos;

      #endregion

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Sequenced Address object
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Type = (CdfTypeID)reader.ReadUInt16();
      ushort length = reader.ReadUInt16();  //Length of the packet is READ-ONLY
      ConnectionID = reader.ReadUInt32();
      SequenceNumber = reader.ReadUInt32();
    }

    #endregion
  }
}
