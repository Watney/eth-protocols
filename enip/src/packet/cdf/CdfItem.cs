﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Cdf Item object
 *  DESCRIPTION:  Generic class to define a Common Data Format item
 */

using UtilByte;
using System.IO;

namespace Enip
{
  /// <summary>
  /// Generic class to define a Common Data Format item
  /// </summary>
  public abstract class CdfItem : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// Code used to identify the item type 
    /// </summary>
    public CdfTypeID Type { get; set; }

    #endregion

    #region CONTRUCTORS

    public CdfItem() : this(CdfTypeID.CDF_NULL)
    {

    }

    public CdfItem(CdfTypeID type)
    {
      Type = type;
    }

    #endregion

    #region IMPLEMENT INTERFACE

    public abstract byte[] Serialize();

    public abstract void Deserialize(MemoryStream stream);

    #endregion
  }
}
