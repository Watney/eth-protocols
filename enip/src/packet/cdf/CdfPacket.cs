﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         31-01-2020
 *  
 *  TITLE:        Cdf Packet object
 *  DESCRIPTION:  Packet used to manage multiple Cdf Items
 */

using UtilByte;
using System;
using System.IO;

namespace Enip
{
  /// <summary>
  /// Packet used to manage multiple Cdf Items
  /// </summary>
  public class CdfPacket : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// List of Cdf items contained in the packet
    /// </summary>
    public CdfItem[] Items { get; private set; }

    /// <summary>
    /// Flag that indicated if serialize items or not
    /// </summary>
    public bool IsSerializable { get; private set; }

    #endregion

    #region CONSTRUCTORS

    public CdfPacket(bool isSerializable = true) : this(new CdfItem[0], isSerializable)
    {

    }

    public CdfPacket(MemoryStream stream)
    {
      IsSerializable = true;
      Deserialize(stream);
    }

    public CdfPacket(CdfItem[] items, bool isSerializable = true)
    {
      Items = items;
      IsSerializable = isSerializable;
    }

    #endregion

    #region IMPLICIT OPERATORS TO CONVERT CDF PACKET IN AN ARRAY OF CDFITEM AND VICEVERSA

    public static implicit operator CdfItem[](CdfPacket packet)
    {
      return packet.Items;
    }

    public static implicit operator CdfPacket(CdfItem[] items)
    {
      return new CdfPacket(items);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Cdf Packet in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      if (IsSerializable)
      {
        ByteWriter writer = new ByteWriter(new MemoryStream());

        writer.Write((ushort)Items.Length);

        foreach (CdfItem item in Items)
          writer.Write(item.Serialize());

        #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

        byte[] result = new byte[writer.Index];
        Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

        #endregion

        return result;
      }
      else
        return new byte[0];
    }

    /// <summary>
    /// Deserialize a byte stream in a Cdf Packet
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      ushort length = reader.ReadUInt16();
      Items = new CdfItem[length];

      for(int i = 0; i < Items.Length; i++)
      {
        CdfTypeID id = (CdfTypeID)reader.ReadUInt16();
        reader.Index -= 2;

        switch(id)
        {
          case CdfTypeID.CDF_NULL: Items[i] = new CdfNullAddress(stream); break;
          case CdfTypeID.CONNECTION_BASED: Items[i] = new CdfConnectedAddress(stream); break;
          case CdfTypeID.CONNECTION_TRANSPORT: Items[i] = new CdfConnectedPacket(stream); break;
          case CdfTypeID.LIST_IDENTITY: Items[i] = new CdfIdentity(stream); break;
          case CdfTypeID.LIST_SERVICE: Items[i] = new CdfService(stream); break;
          case CdfTypeID.SEQ_ADDRESS: Items[i] = new CdfSequencedAddress(stream); break;
          case CdfTypeID.SOCK_ADDR_INFO_OT: Items[i] = new CdfSocketAddressOT(stream); break;
          case CdfTypeID.SOCK_ADDR_INFO_TO: Items[i] = new CdfSocketAddressTO(stream); break;
          case CdfTypeID.UNCONNECTED_MSG: Items[i] = new CdfUnconnectedPacket(stream); break;
          default: Items[i] = null; break;
        }
      }
    }

    #endregion
  }
}
