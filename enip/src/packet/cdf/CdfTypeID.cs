﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         14-01-2020
 *  
 *  TITLE:        Cdf Type ID enum
 *  DESCRIPTION:  Code used to identify the common data format sended/returned in a ethernet-ip communication
 */

namespace Enip
{
  /// <summary>
  /// Code used to identify the common data format sended/returned in a ethernet-ip communication
  /// </summary>
  public enum CdfTypeID
  {
    CDF_NULL = 0x0000,
    LIST_IDENTITY = 0x000C,
    CONNECTION_BASED = 0x00A1,
    CONNECTION_TRANSPORT = 0x00B1,
    UNCONNECTED_MSG = 0x00B2,
    LIST_SERVICE = 0x0100,
    SOCK_ADDR_INFO_OT = 0x8000,
    SOCK_ADDR_INFO_TO = 0x8001,
    SEQ_ADDRESS = 0x8002
  }
}
