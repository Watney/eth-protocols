﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         18-02-2020
 *  
 *  TITLE:        Capability Flags struct
 *  DESCRIPTION:  Specifies which protocols are supported for a service
 */

namespace Enip
{
  /// <summary>
  /// Specifies which protocols are supported for a service
  /// </summary>
  public struct CapabilityFlags
  {
    #region ATTRIBUTES

    /// <summary>
    /// Indicates if device supports TCP protocol
    /// </summary>
    public bool IsTcpSupported { get; set; }

    /// <summary>
    /// Indicates if device supports UDP protocol
    /// </summary>
    public bool IsUdpSupported { get; set; }
    
    #endregion

    public static explicit operator ushort(CapabilityFlags flags)
    {
      int tcp = (flags.IsTcpSupported ? 1 : 0) << 5;
      int udp = (flags.IsUdpSupported ? 1 : 0) << 8;

      return (ushort)(tcp | udp);
    }

    public static explicit operator CapabilityFlags(ushort flags)
    {
      CapabilityFlags capFlags = new CapabilityFlags()
      {
        IsTcpSupported = ((flags & 0x20) >> 5) > 0,
        IsUdpSupported = (flags >> 8) > 0
      };

      return capFlags;
    }
  }
}
