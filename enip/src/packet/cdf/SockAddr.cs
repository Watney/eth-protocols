﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Socket Address object
 *  DESCRIPTION:  Structure used to define the socket settings of a device  
 */

using UtilByte;
using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;

namespace Enip
{
  /// <summary>
  /// Structure used to define the socket settings of a device
  /// </summary>
  public struct SockAddr : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// Indicates which type of addressing use the device
    /// </summary>
    public AddressFamily Family { get; set; }

    /// <summary>
    /// Port number where the device is available
    /// </summary>
    public ushort Port { get; set; }

    /// <summary>
    /// Ip address where the device is available
    /// </summary>
    public string IPAddress { get; set; }

    /// <summary>
    /// Unused bytes
    /// </summary>
    public ulong Zero => 0;

    #endregion

    #region CONSTRUCTORS

    public SockAddr(MemoryStream stream) : this()
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Socket Address object in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((ushort)Family);
      writer.Write(Port);
      #region SERIALIZE IP ADDRESS

      List<byte> ipAddr = new List<byte>();

      foreach (string ipField in IPAddress.Split('.'))
        ipAddr.Add(byte.Parse(ipField));

      writer.Write(ipAddr.ToArray());

      #endregion
      writer.Write(Zero);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in a Socket Address object  
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream, true);

      Family = (AddressFamily)reader.ReadUInt16();
      Port = reader.ReadUInt16();
      #region DESERIALIZE IP ADDRESS

      byte[] ipAddr = reader.ReadBytes(4);

      IPAddress = string.Format("{0}.{1}.{2}.{3}", ipAddr[0], ipAddr[1], ipAddr[2], ipAddr[3]);

      #endregion
      ulong zero = reader.ReadUInt64();
    }

    #endregion
  }
}
