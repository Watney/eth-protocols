﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Enip Packet object
 *  DESCRIPTION:  Packet object used to pack/unpack data using ethernet-ip protocol
 */

using UtilByte;
using System.IO;
using System;

namespace Enip
{
  /// <summary>
  /// Packet object used to pack/unpack data using ethernet-ip protocol
  /// </summary>
  public class EnipPacket : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// Ethernet-ip command sent or received 
    /// </summary>
    public EnipCommand Command { get; set; }

    /// <summary>
    /// Code that identifies the current ethernet-ip communication 
    /// </summary>
    public uint SessionHandle { get; set; }

    /// <summary>
    /// Status of the request/response packet sent or received
    /// </summary>
    public EnipStatus Status { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public ulong SenderContext { get; set; }

    /// <summary>
    /// Optional flags
    /// </summary>
    public uint Options { get; set; }

    #endregion

    #region CONSTRUCTORS

    public EnipPacket(EnipCommand command, uint sessionHandle, EnipStatus status, ulong senderContext)
    {
      Command = command;
      SessionHandle = sessionHandle;
      Status = status;
      SenderContext = SenderContext;
    }

    public EnipPacket(byte[] buffer)
    {
      Deserialize(new MemoryStream(buffer));
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Enip Packet in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((ushort)Command.Code);
      writer.Write((ushort)Command.Serialize().Length);
      writer.Write(SessionHandle);
      writer.Write((uint)Status);
      writer.Write(SenderContext);
      writer.Write(Options);
      writer.Write(Command.Serialize());

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Enip Packet
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      EnipCommandCode code = (EnipCommandCode)reader.ReadUInt16();
      ushort length = reader.ReadUInt16();  //  Field Length is READ-ONLY
      SessionHandle = reader.ReadUInt32();
      Status = (EnipStatus)reader.ReadUInt32();
      SenderContext = reader.ReadUInt64();
      Options = reader.ReadUInt32();

      if(Status == EnipStatus.EIP_SUCCESS)
      {
        switch (code)
        {
          case EnipCommandCode.RegisterSession: Command = new EnipRegisterSession(stream); break;
          case EnipCommandCode.UnRegisterSession: Command = new EnipUnregisterSession(stream); break;
          case EnipCommandCode.ListIdentity: Command = new EnipListIdentity(stream); break;
          case EnipCommandCode.ListServices: Command = new EnipListService(stream); break;
          case EnipCommandCode.SendRRData: Command = new EnipSendRRData(stream); break;
          case EnipCommandCode.SendUnitData: Command = new EnipSendUnitData(stream); break;
          case EnipCommandCode.Nop: Command = new EnipNop(stream); break;
          default: Command = null; break;
        }
      }
      else
        Command = null;
    }

    #endregion
  }
}
