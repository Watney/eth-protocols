﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Enip List Service Command object
 *  DESCRIPTION:  Command used to retreive the list of Service stored in the device
 */

using UtilByte;
using System;
using System.IO;

namespace Enip
{
  /// <summary>
  /// Command used to retreive the list of Service stored in the device
  /// </summary>
  public class EnipListService : EnipCommand
  {
    #region ATTRIBUTES

    /// <summary>
    /// List of services located in the device
    /// </summary>
    public CdfPacket Services { get; private set; }

    #endregion

    #region CONSTRUCTORS

    public EnipListService() : this(new CdfPacket(false))
    {

    }

    public EnipListService(MemoryStream stream) : base(EnipCommandCode.ListServices)
    {
      Deserialize(stream);
    }

    public EnipListService(CdfPacket services) : base(EnipCommandCode.ListServices)
    {
      Services = services;
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the List Service Command in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      if (Services.IsSerializable)
        writer.Write(Services.Serialize());

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the List Service Command
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      Services = new CdfPacket(stream);
    }

    #endregion
  }
}
