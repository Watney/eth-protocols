﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         12-01-2020
 *  
 *  TITLE:        Enip Command Code enum
 *  DESCRIPTION:  Code used to identify the ethernet-ip request/response command performed
 */

namespace Enip
{
  /// <summary>
  /// Code used to identify the ethernet-ip request/response command performed
  /// </summary>
  public enum EnipCommandCode : ushort
  {
    Nop = 0x0000,
    ListServices = 0x0004,
    ListIdentity = 0x0063,
    ListInterfaces = 0x0064,
    RegisterSession = 0x0065,
    UnRegisterSession = 0x0066,
    SendRRData = 0x006F,
    SendUnitData = 0x0070,
    IndicateStatus = 0x0072,
    Cancel = 0x0073
  }
}
