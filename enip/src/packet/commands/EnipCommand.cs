﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         23-01-2020
 *  
 *  TITLE:        Enip Command object
 *  DESCRIPTION:  Generic class to define a command sub-packet
 */

using UtilByte;
using System.IO;

namespace Enip
{
  /// <summary>
  /// Generic class to define a command sub-packet
  /// </summary>
  public abstract class EnipCommand : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// Code used to identify the command
    /// </summary>
    public EnipCommandCode Code { get; protected set; }

    #endregion

    #region CONSTRUCTORS

    public EnipCommand() : this(EnipCommandCode.Nop)
    { 

    }

    public EnipCommand(EnipCommandCode code)
    {
      Code = code;
    }

    #endregion

    #region IMPLEMENT INTERFACE

    public abstract byte[] Serialize();

    public abstract void Deserialize(MemoryStream stream);

    #endregion
  }
}
