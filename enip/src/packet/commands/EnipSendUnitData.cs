﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Enip Send Unit Data Command object
 *  DESCRIPTION:  Command used to send request/response packet in a connected communication
 */

using UtilByte;
using System;
using System.IO;

namespace Enip
{
  /// <summary>
  /// Command used to send request/response packet in a connected communication
  /// </summary>
  public class EnipSendUnitData : EnipCommand
  {
    public const uint DEFAULT_INTERFACE_HANDLE = 0;
    public const ushort DEFAULT_TIMEOUT = 0;

    #region ATTRIBUTES

    /// <summary>
    /// Code that indicates the interface used in the communication
    /// </summary>
    public uint InterfaceHandle { get; set; }

    /// <summary>
    /// Sending timeout of the packet
    /// </summary>
    public ushort Timeout { get; set; }

    /// <summary>
    /// Data to be transmitted
    /// </summary>
    public CdfPacket Packet { get; set; }

    #endregion

    #region CONSTRUCTORS

    public EnipSendUnitData(CdfPacket packet) : this(packet, DEFAULT_INTERFACE_HANDLE, DEFAULT_TIMEOUT)
    {

    }

    public EnipSendUnitData(CdfPacket packet, uint interfaceHandle, ushort timeout) : base(EnipCommandCode.SendUnitData)
    {
      Packet = packet;
      InterfaceHandle = interfaceHandle;
      Timeout = timeout;
    }

    public EnipSendUnitData(MemoryStream stream) : base(EnipCommandCode.SendUnitData)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Cdf Send Unit Data command in a byte stream 
    /// </summary>
    public override byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(InterfaceHandle);
      writer.Write(Timeout);
      writer.Write(Packet.Serialize());

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Cdf Send Unit Data command 
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      InterfaceHandle = reader.ReadUInt32();
      Timeout = reader.ReadUInt16();
      Packet = new CdfPacket(stream);
    }

    #endregion
  }
}