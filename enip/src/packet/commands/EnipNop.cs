﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         23-01-2020
 *  
 *  TITLE:        Enip Nop Command object
 *  DESCRIPTION:  Command used to send generic data using ethernet-ip protocol
 */

using UtilByte;
using System.IO;

namespace Enip
{
  /// <summary>
  /// Command used to send generic data using ethernet-ip protocol
  /// </summary>
  public class EnipNop : EnipCommand
  {
    #region ATTRIBUTES

    /// <summary>
    /// Generic data
    /// </summary>
    public byte[] Data { get; set; }

    #endregion

    #region CONSTRUCTORS

    public EnipNop(byte[] data) : this(new MemoryStream(data))
    {

    }

    public EnipNop(MemoryStream stream) : base(EnipCommandCode.Nop)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the command object in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      return Data;
    }

    /// <summary>
    /// Deserialize a byte stream in a command object
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Data = reader.ReadBytes(stream.Capacity);
    }

    #endregion
  }
}
