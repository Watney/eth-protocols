﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Enip Register Session Command object
 *  DESCRIPTION:  Command used to init the ethernet-ip communication between 2 devices
 */

using UtilByte;
using System;
using System.IO;

namespace Enip
{
  /// <summary>
  /// Command used to init the ethernet-ip communication between 2 devices
  /// </summary>
  public class EnipRegisterSession : EnipCommand
  {
    public const ushort DEFAULT_PROTOCOL_VERSION = 1;
    public const ushort DEFAULT_OPTION_FLAGS = 0;

    #region ATTRIBUTES

    /// <summary>
    /// Version of the ethernet-ip protocol used in the communication
    /// </summary>
    public ushort ProtocolVersion { get; set; }

    /// <summary>
    /// Optional flags
    /// </summary>
    public ushort OptionFlags { get; set; }

    #endregion

    #region CONSTRUCTORS

    public EnipRegisterSession() : this(DEFAULT_PROTOCOL_VERSION, DEFAULT_OPTION_FLAGS)
    {

    }

    public EnipRegisterSession(MemoryStream stream) : base(EnipCommandCode.RegisterSession)
    {
      Deserialize(stream);
    }

    public EnipRegisterSession(ushort protocolVersion, ushort optionFlags) : base(EnipCommandCode.RegisterSession)
    {
      ProtocolVersion = protocolVersion;
      OptionFlags = optionFlags;
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the command object in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(ProtocolVersion);
      writer.Write(OptionFlags);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in a command object
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      ProtocolVersion = reader.ReadUInt16();
      OptionFlags = reader.ReadUInt16();
    }

    #endregion
  }
}
