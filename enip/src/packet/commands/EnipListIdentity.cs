﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         28-01-2020
 *  
 *  TITLE:        Enip List Identity Command object
 *  DESCRIPTION:  Command used to retreive the list of Identity stored in the device (typically 1)
 */

using UtilByte;
using System;
using System.IO;

namespace Enip
{
  /// <summary>
  /// Command used to retreive the list of Identity stored in the device (typically 1)
  /// </summary>
  public class EnipListIdentity : EnipCommand
  {
    #region ATTRIBUTES

    /// <summary>
    /// List of identies located in the device
    /// </summary>
    public CdfPacket Identities { get; private set; }

    #endregion

    #region CONSTRUCTORS

    public EnipListIdentity() : this(new CdfPacket(false))
    {

    }

    public EnipListIdentity(MemoryStream stream) : base(EnipCommandCode.ListIdentity)
    {
      Deserialize(stream);
    }

    public EnipListIdentity(CdfPacket identities) : base(EnipCommandCode.ListIdentity)
    {
      Identities = identities;
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the List Identity Command in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      if (Identities.IsSerializable)
        writer.Write(Identities.Serialize());

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the List Identity Command
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
      Identities = new CdfPacket(stream);
    }

    #endregion
  }
}
