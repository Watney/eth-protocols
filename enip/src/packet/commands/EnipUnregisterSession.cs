﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         23-01-2020
 *  
 *  TITLE:        Enip Unregister Session Command object
 *  DESCRIPTION:  Command used to end the ethernet-ip communication between 2 devices
 */

using System.IO;

namespace Enip
{
  /// <summary>
  /// Command used to end the ethernet-ip communication between 2 devices
  /// </summary>
  class EnipUnregisterSession : EnipCommand
  {
    #region CONSTRUCTORS

    public EnipUnregisterSession() : base(EnipCommandCode.UnRegisterSession)
    {

    }

    public EnipUnregisterSession(MemoryStream stream) : base(EnipCommandCode.UnRegisterSession)
    {
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the command object in a byte stream
    /// </summary>
    public override byte[] Serialize()
    {
      return new byte[0];
    }

    /// <summary>
    /// Deserialize a byte stream in a command data object
    /// </summary>
    public override void Deserialize(MemoryStream stream)
    {
    }

    #endregion
  }
}
