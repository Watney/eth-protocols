﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         12-01-2020
 *  
 *  TITLE:        Enip Status enum
 *  DESCRIPTION:  List of every possible ethernet-ip response status
 */

namespace Enip
{
  /// <summary>
  /// List of every possible ethernet-ip response status
  /// </summary>
  public enum EnipStatus : uint
  {
    EIP_SUCCESS = 0x0000,
    INVALID_UNSUPPORTED = 0x0001,
    INSUFFICIENT_MEMORY = 0x0002,
    MALFORMED_DATA = 0x0003,
    INVALID_SESSION_HANDLE = 0x0064,
    INVALID_LENGTH = 0x0065,
    UNSUPPORTED_PROTOCOL_VERSION = 0x0069
  }
}
