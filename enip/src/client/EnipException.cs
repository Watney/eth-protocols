﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         12-01-2020
 *  
 *  TITLE:        Enip Exception object
 *  DESCRIPTION:  Object used to manage Enip Exceptions
 */

using System;
using System.Collections.Generic;

namespace Enip
{
  /// <summary>
  /// Object used to manage Enip Exceptions
  /// </summary>
  public class EnipException : Exception
  {
    private static Dictionary<EnipStatus, string> Messages = new Dictionary<EnipStatus, string>()
    {
      { EnipStatus.EIP_SUCCESS, "Success" },
      { EnipStatus.INVALID_UNSUPPORTED, "Invalid Command" },
      { EnipStatus.INSUFFICIENT_MEMORY, "No Memory Resources" },
      { EnipStatus.MALFORMED_DATA, "Incorrect Data" },
      { EnipStatus.INVALID_SESSION_HANDLE, "Invalid Session Handle" },
      { EnipStatus.INVALID_LENGTH, "Invalid Length" },
      { EnipStatus.UNSUPPORTED_PROTOCOL_VERSION, "Unsupported Protocol Revision"}
    };

    #region ATTRIBUTES

    /// <summary>
    /// Status code of the response packet 
    /// </summary>
    public EnipStatus Status { get; private set; }

    #endregion

    #region CONSTRUCTORS

    public EnipException(EnipStatus status) : base(Messages[status])
    {
      Status = status;
    }

    #endregion
  }
}
