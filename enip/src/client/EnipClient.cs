﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         12-02-2020
 *  
 *  TITLE:        Enip Client object
 *  DESCRIPTION:  Synchronous Ethernet-IP Client
 */

using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Net.NetworkInformation;

namespace Enip
{
  /// <summary>
  /// Synchronous Ethernet-IP Client 
  /// </summary>
  public class EnipClient
  {
    public const ushort DEFAULT_PORT = 44818;
    public const uint DEFAULT_SESSION_HANDLE = 0;

    #region ATTRIBUTES

    /// <summary>
    /// IP Address of the device to connect
    /// </summary>
    public string IpAddress { get; set; }

    /// <summary>
    /// Port number of the device for ethernet-ip communications
    /// </summary>
    public ushort Port { get; set; }

    /// <summary>
    /// Code used to identify the current ethernet-ip session
    /// </summary>
    public uint SessionHandle { get; private set; }

    private Socket connSocket;

    /// <summary>
    /// Indicates the connection status of the client
    /// </summary>
    public bool IsConnected => (connSocket != null) ? connSocket.Connected : false;

    #endregion

    #region CONSTRUCTORS

    public EnipClient(string ipAddress) : this(ipAddress, DEFAULT_PORT)
    {

    }

    public EnipClient(string ipAddress, ushort port)
    {
      IpAddress = ipAddress;
      Port = port;
      SessionHandle = DEFAULT_SESSION_HANDLE;
    }

    #endregion

    #region CONNECTION FUNCTIONS

    /// <summary>
    /// Connect to device and obtain Session Handle
    /// </summary>
    public void Connect()
    {
      bool connected = true;

      #region PING REMOTE DEVICE

      Ping ping = new Ping();
      PingReply reply = ping.Send(IpAddress);

      connected = reply.Status == IPStatus.Success;

      #endregion

      #region OPEN A SOCKET CONNECTION TO THE REMOTE DEVICE

      if (connected)
      {
        connSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        connSocket.Connect(IPAddress.Parse(IpAddress), Port);

        connected = connSocket.Connected;
      }

      #endregion

      #region SEND A REGISTER SESSION PACKET TO REMOTE DEVICE TO OBTAIN SESSION-HANDLE

      if (connected)
      {
        //  Save the session handler for future communications  
        EnipPacket resSession = SendData(new EnipRegisterSession());
        SessionHandle = resSession.SessionHandle;
      }

      #endregion
    }

    /// <summary>
    /// Disconnect from device
    /// </summary>
    public void Disconnect()
    {
      //  Unregister from ethernet-ip communication
      SendData(new EnipUnregisterSession(), false);

      //  Close connection socket
      connSocket.Close();
    }

    #endregion

    #region INFO FUNCTIONS

    /// <summary>
    /// Get the list of all identities stored in device
    /// </summary>
    public CdfIdentity[] GetIdentities()
    {
      List<CdfIdentity> identities = new List<CdfIdentity>();

      //  Request of all services stored in device
      EnipPacket resIdentities = SendData(new EnipListIdentity());

      //  Obtain from response packet the list of identities
      foreach (CdfItem item in (resIdentities.Command as EnipListIdentity).Identities.Items)
        identities.Add((CdfIdentity)item);

      return identities.ToArray();
    }

    /// <summary>
    /// Get the list of all services stored in device
    /// </summary>
    public CdfService[] GetServices()
    {
      List<CdfService> services = new List<CdfService>();

      //  Request of all services stored in device
      EnipPacket resServices = SendData(new EnipListService());

      //  Obtain from response packet the list of services
      foreach (CdfItem item in (resServices.Command as EnipListService).Services.Items)
        services.Add((CdfService)item);

      return services.ToArray();
    }

    /// <summary>
    /// Get the list of all identities in the network
    /// </summary>
    public static CdfIdentity[] GetAllIdentities(int timeout = 5000)
    {
      List<CdfIdentity> identities = new List<CdfIdentity>();

      //  Define UDP socket to send a broadcast packet to the broadcast address in the network
      Socket socket = new Socket(SocketType.Dgram, ProtocolType.Udp);
      socket.EnableBroadcast = true;
      socket.ReceiveTimeout = timeout;

      //  Send a broadcast packet to retreive all identities in the network
      EnipPacket packet = new EnipPacket(new EnipListIdentity(), 0, EnipStatus.EIP_SUCCESS, 0);
      socket.SendTo(packet.Serialize(), new IPEndPoint(IPAddress.Broadcast, DEFAULT_PORT));

      bool err = false;
      while (!err)
      {
        byte[] buff = new byte[65535];
        EnipPacket recv;

        try
        {
          socket.Receive(buff);
          recv = new EnipPacket(buff);

          //  Obtain from response packet the list of identities
          foreach (CdfItem item in (recv.Command as EnipListIdentity).Identities.Items)
            identities.Add((CdfIdentity)item);
        }
        catch { err = true; }
      }

      return identities.ToArray();
    }

    #endregion

    #region SEND DATA FUNCTIONS

    /// <summary>
    /// Send an not connected packet using ethernet-ip protocol
    /// </summary>
    /// <param name="command">Command to send</param>
    /// <param name="isRequest">Indicates if the functions has to return a response packet</param>
    /// <returns>Returns a Response Packet if the function is of request type</returns>
    protected EnipPacket SendRRPacket(EnipSendRRData command, bool isRequest = true)
    {
      return SendData(command, isRequest);
    }

    /// <summary>
    /// Send an connected packet using ethernet-ip protocol
    /// </summary>
    /// <param name="command">Command to send</param>
    /// <param name="isRequest">Indicates if the functions has to return a response packet</param>
    /// <returns>Returns a Response Packet if the function is of request type</returns>
    protected EnipPacket SendUnitPacket(EnipSendRRData command, bool isRequest = true)
    {
      return SendData(command, isRequest);
    }

    private EnipPacket SendData(EnipCommand command, bool isRequest = true)
    {
      EnipPacket resPacket = null;
      EnipPacket reqPacket = new EnipPacket(command, SessionHandle, EnipStatus.EIP_SUCCESS, 0);

      connSocket.Send(reqPacket.Serialize());

      if (isRequest)
      {
        byte[] response = new byte[65535];
        connSocket.Receive(response);

        resPacket = new EnipPacket(response);

        if (resPacket.Status != EnipStatus.EIP_SUCCESS)
          throw new EnipException(resPacket.Status);
      }

      return resPacket;
    }

    #endregion
  }
}
