﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         11-02-2020
 *  
 *  TITLE:        Omron Tag Name Server object
 *  DESCRIPTION:  Omron specific object used to retreive public variable tags from PLC
 */

using UtilByte;
using System;
using System.IO;
using System.Text;

namespace Omron
{
  /// <summary>
  /// Omron specific object used to retreive public variable tags from PLC
  /// </summary>
  public class OmronTag : ISerializable
  {
    #region ATTRIBUTES

    /// <summary>
    /// Instance identifier (used for Omron Variable object) 
    /// </summary>
    public ushort InstanceID { get; set; }

    /// <summary>
    /// Symbolic name of the tag
    /// </summary>
    public string SymbolicName { get; set; }

    private bool IsSingle { get; set; }

    #endregion

    #region CONSTRUCTORS

    public OmronTag(string name, ushort instanceId, bool isSingle = true)
    {
      SymbolicName = name;
      InstanceID = instanceId;
      IsSingle = isSingle;
    }

    public OmronTag(MemoryStream stream, bool isSingle = true)
    {
      IsSingle = isSingle;
      Deserialize(stream);
    }

    #endregion

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Omron Tag Nmae Server object in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(new byte[] { 0x6B, 0x00 });

      if (IsSingle)
        writer.Write(InstanceID);
      else
        writer.Write((uint)InstanceID);

      writer.Write((byte)SymbolicName.Length);
      writer.Write(Encoding.UTF8.GetBytes(SymbolicName));
      if (SymbolicName.Length % 2 == 0) writer.Write((byte)0);

      writer.Write((uint)0);
      if (IsSingle)
        writer.Write((ushort)0);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Omron Tag Name Server object
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      reader.ReadBytes(2);

      InstanceID = (ushort)(IsSingle ? reader.ReadUInt16() : reader.ReadUInt32());

      byte length = reader.ReadByte();
      SymbolicName = Encoding.UTF8.GetString(reader.ReadBytes(length));
      if (length % 2 == 0) reader.ReadByte();

      if ((reader.BaseStream as MemoryStream).CanRead)
      {
        reader.ReadBytes(4);
        if (!IsSingle) reader.ReadBytes(2);
      }
    }

    #endregion
  }
}
