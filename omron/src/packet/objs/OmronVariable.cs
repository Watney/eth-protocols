﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         14-02-2020
 *  
 *  TITLE:        Omron Variable object
 *  DESCRIPTION:  Omron specific object used to specifies variable tags data
 */

using UtilByte;
using Cip;
using System;
using System.IO;
using System.Linq;

namespace Omron
{
  /// <summary>
  /// Omron specific object used to specifies variable tags data
  /// </summary>
  public class OmronVariable : ISerializable
  {
    public const byte TAG_STRUCT = 0xA2;
    public const byte TAG_ARRAY = 0xA3;

    #region ATTRIBUTES

    /// <summary>
    /// Byte size of the tag variable
    /// </summary>
    public uint Size { get; set; }

    /// <summary>
    /// Identifier code that specifies the type of the tag variable
    /// </summary>
    public byte TagType { get; set; }

    /// <summary>
    /// Identifier code that specifies the second type of the tag variable (used if the variable is an array)
    /// </summary>
    public byte SecondaryTagType { get; set; }

    /// <summary>
    /// Dimensions size of the variable (used if the variable is an array)
    /// </summary>
    public ushort Dimensions { get; set; }

    /// <summary>
    /// Length of the tag variable (used if the variable is an array) 
    /// </summary>
    public uint Length { get; set; }

    /// <summary>
    /// Instance identifier (used for Omron Variable Type object) 
    /// </summary>
    public ushort InstanceID { get; set; }

    #endregion

    #region CONSTRUCTORS

    public OmronVariable(OmronData data, ushort instance)
    {
      if (data is OmronArray)
      {
        OmronArray arr = data as OmronArray;

        TagType = TAG_ARRAY;
        SecondaryTagType = (arr.EmbeddedData is OmronAtomic)
          ? (byte)CipDataManager.Types.First(t => t.Value == (data as OmronAtomic).Type).Key
          : TagType = TAG_STRUCT;

        Dimensions = arr.Dimensions;
        Length = arr.Length;
      }
      else
      {
        TagType = (data is OmronAtomic)
          ? (byte)CipDataManager.Types.First(t => t.Value == (data as OmronAtomic).Type).Key
          : TagType = TAG_STRUCT;

        SecondaryTagType = 0;     
        Dimensions = 0;
        Length = 0;
      }

      Size = (uint)data.Size;
      InstanceID = instance;
    }

    public OmronVariable(MemoryStream stream)
    {
      Deserialize(stream);
    }

    #endregion

    public bool IsArray => TagType == TAG_ARRAY;
    public bool IsStruct => TagType == TAG_STRUCT || (TagType == TAG_ARRAY && SecondaryTagType == TAG_STRUCT);

    #region IMPLEMENT INTERFACES

    /// <summary>
    /// Serialize the Omron Variable object in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(Size);
      writer.Write(TagType);

      if (TagType == TAG_ARRAY)
      {
        writer.Write(SecondaryTagType);
        writer.Write(Dimensions);
        writer.Write(Length);
      }
      else
      {
        writer.Write((byte)0);
        writer.Write((byte)0);
        writer.Write((byte)0);
      }

      writer.Write(InstanceID);
      writer.Write(new byte[]
      {
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x04,
        0x00, 0x00
      });

      writer.Write(InstanceID);
      writer.Write((ushort)0);

      if (TagType == TAG_ARRAY)
        writer.Write(new byte[] { 0x01, 0x00, 0x00, 0x00 });

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Omron Variable object
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Size = reader.ReadUInt32();
      TagType = reader.ReadByte();
      SecondaryTagType = reader.ReadByte();

      if (TagType == TAG_ARRAY)
      {
        Dimensions = reader.ReadUInt16();
        Length = reader.ReadUInt32();
      }
      else
      {
        Dimensions = reader.ReadByte();
        Length = reader.ReadByte();
      }

      InstanceID = reader.ReadUInt16();

      reader.ReadBytes(10);
      reader.ReadBytes(2);
      reader.ReadBytes(2);

      if (TagType == TAG_ARRAY)
        reader.ReadBytes(4);
    }

    #endregion
  }
}
