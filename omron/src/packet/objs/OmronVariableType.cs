﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         14-02-2020
 *  
 *  TITLE:        Omron Variable Type object
 *  DESCRIPTION:  Omron specific object used to specifies variable type structure 
 */

using UtilByte;
using Cip;
using System;
using System.IO;
using System.Text;
using System.Linq;

namespace Omron
{
  /// <summary>
  /// Omron specific object used to specifies variable type structure 
  /// </summary>
  public class OmronVariableType : ISerializable
  {
    public const byte TAG_STRUCT = 0xA2;
    public const byte TAG_ARRAY = 0xA3;

    #region ATTRIBUTES

    /// <summary>
    /// Byte size of the variable type
    /// </summary>
    public uint Size { get; set; }

    /// <summary>
    /// Identifier code that specifies the type of the tag variable
    /// </summary>
    public byte TagType { get; set; }

    /// <summary>
    /// Identifier code that specifies the second type of the tag variable (used if the variable is an array)
    /// </summary>
    public byte SecondaryTagType { get; set; }

    /// <summary>
    /// Dimensions size of the variable (used if the variable is an array)
    /// </summary>
    public ushort Dimensions { get; set; }

    /// <summary>
    /// Length of the tag variable (used if the variable is an array) 
    /// </summary>
    public uint Length { get; set; }

    /// <summary>
    /// Indicates the count of fields of that variable 
    /// </summary>
    public ushort FieldsCount { get; set; }

    /// <summary>
    /// Indicates the instance id of the next field in the struct
    /// </summary>
    public ushort NextField { get; set; }

    /// <summary>
    /// Instance identifier (used if a strcuture contains another structure) 
    /// </summary>
    public ushort InstanceID { get; set; }

    /// <summary>
    /// Identifier code of the variable type
    /// </summary>
    public ushort StructID { get; set; }

    /// <summary>
    /// Symbolic name of the variable type
    /// </summary>
    public string SymbolicName { get; set; }

    #endregion

    #region CONSTRUCTORS

    public OmronVariableType(OmronData data, ushort instance, string name, ushort nextField = 0)
    {
      if (data is OmronArray)
      {
        OmronArray arr = data as OmronArray;

        TagType = TAG_ARRAY;
        SecondaryTagType = (arr.EmbeddedData is OmronAtomic)
          ? (byte)CipDataManager.Types.First(t => t.Value == (data as OmronAtomic).Type).Key
          : TagType = TAG_STRUCT;

        SymbolicName = name;
        Dimensions = arr.Dimensions;
        Length = arr.Length;
        FieldsCount = 0;
        StructID = 0;
      }
      else
      {
        if(data is OmronAtomic)
        {
          TagType = (byte)CipDataManager.Types.First(t => t.Value == (data as OmronAtomic).Type).Key;
          SymbolicName = name;
          FieldsCount = 0;
          StructID = 0;
        }
        else
        {
          OmronStruct str = data as OmronStruct;

          // "Next field" <> 0 means struct instance inside another struct
          TagType = TAG_STRUCT;
          SymbolicName = (nextField == 0) ? str.SymbolicName : name;
          FieldsCount = (ushort)((nextField == 0) ? str.Fields.Count : 0);
          StructID = (ushort)((nextField == 0) ? str.Id : 0);
        }       

        SecondaryTagType = 0;
        Dimensions = 0;
        Length = 0;
      }

      NextField = nextField;
      Size = (uint)data.Size;
      InstanceID = instance;
    }

    public OmronVariableType(MemoryStream stream)
    {
      Deserialize(stream);
    }

    #endregion

    public bool IsArray => TagType == TAG_ARRAY;
    public bool IsStruct => TagType == TAG_STRUCT || (TagType == TAG_ARRAY && SecondaryTagType == TAG_STRUCT);
    public bool IsField => StructID == 0;

    #region IMPLEMENT INTERFACE

    /// <summary>
    /// Serialize the Omron Variable Type object in a byte stream
    /// </summary>
    public byte[] Serialize()
    {
      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(Size);
      writer.Write((byte)0);
      writer.Write(TagType);

      if (TagType == TAG_ARRAY)
      {
        writer.Write(SecondaryTagType);
        writer.Write(Dimensions);
        writer.Write(Length);
      }
      else
      {
        writer.Write((byte)0);
        writer.Write((byte)0);
      }

      writer.Write(FieldsCount);
      writer.Write(NextField);
      writer.Write(InstanceID);
      writer.Write(StructID);

      writer.Write((byte)SymbolicName.Length);
      writer.Write(Encoding.UTF8.GetBytes(SymbolicName));
      if (SymbolicName.Length % 2 == 0) writer.Write((byte)0);

      writer.Write((uint)NextField);
      writer.Write((uint)InstanceID);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] result = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), result, result.Length);

      #endregion

      return result;
    }

    /// <summary>
    /// Deserialize a byte stream in the Omron Variable Type object
    /// </summary>
    public void Deserialize(MemoryStream stream)
    {
      ByteReader reader = new ByteReader(stream);

      Size = reader.ReadUInt32();
      reader.ReadByte();
      TagType = reader.ReadByte();

      if (TagType == TAG_ARRAY)
      {
        SecondaryTagType = reader.ReadByte();
        Dimensions = reader.ReadByte();
        Length = reader.ReadUInt32();
      }
      else
      {
        reader.ReadByte();
        reader.ReadByte();
      }

      FieldsCount = reader.ReadUInt16();
      NextField = reader.ReadUInt16();
      InstanceID = reader.ReadUInt16();
      StructID = reader.ReadUInt16();

      byte length = reader.ReadByte();
      SymbolicName = Encoding.UTF8.GetString(reader.ReadBytes(length));
      if (length % 2 == 0) reader.ReadByte();

      reader.ReadUInt32();
      reader.ReadUInt32();
    }

    #endregion
  }
}
