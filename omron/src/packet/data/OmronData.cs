﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         13-02-2020
 *  
 *  TITLE:        Omron Data object
 *  DESCRIPTION:  Generic class used to define data in Omron PLCs 
 */

namespace Omron
{
  /// <summary>
  /// Generic class used to define data in Omron PLCs 
  /// </summary>
  public abstract class OmronData
  {
    #region ATTRIBUTES

    /// <summary>
    /// Byte size of the data 
    /// </summary>
    public int Size { get; set; }

    #endregion

    #region CONSTRUCTORS

    public OmronData(int size)
    {
      Size = size;
    }

    #endregion
  }
}
