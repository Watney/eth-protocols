﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         13-02-2020
 *  
 *  TITLE:        Omron Atomic object
 *  DESCRIPTION:  Specifies an atomic data in Omron PLCs
 */

using Cip;
using System;

namespace Omron
{
  /// <summary>
  /// Specifies an atomic data in Omron PLCs
  /// </summary>
  public class OmronAtomic : OmronData
  {
    #region ATTRIBUTES

    /// <summary>
    /// Data type
    /// </summary>
    public Type Type { get; set; }

    #endregion

    #region CONSTRUCTORS

    public OmronAtomic(Type type, int size) : base(size)
    {
      Type = type;
    }

    public OmronAtomic(byte tag, int size) : this(CipDataManager.Types[(CipTagType)tag], size)
    {
    }

    #endregion
  }
}
