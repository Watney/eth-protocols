﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         13-02-2020
 *  
 *  TITLE:        Omron Struct object
 *  DESCRIPTION:  Specifies a structure data in Omron PLCs
 */

using System.Collections.Generic;

namespace Omron
{
  /// <summary>
  /// Specifies a structure data in Omron PLCs
  /// </summary>
  public class OmronStruct : OmronData
  {
    #region ATTRIBUTES

    /// <summary>
    /// Symbolic name of the structure
    /// </summary>
    public string SymbolicName { get; set; }

    /// <summary>
    /// Identifier code of the structure data
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Array of structure's fields
    /// </summary>
    public Dictionary<string, OmronData> Fields { get; private set; }

    #endregion

    #region CONSTRUCTORS

    public OmronStruct(string name, int id, int fieldsCount, int size) : base(size)
    {
      SymbolicName = name;
      Id = id;
      Fields = new Dictionary<string, OmronData>(fieldsCount);
    }

    #endregion
  }
}
