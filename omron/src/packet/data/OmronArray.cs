﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         19-02-2020
 *  
 *  TITLE:        Omron Atomic object
 *  DESCRIPTION:  Specifies an atomic data in Omron PLCs
 */

namespace Omron
{
  /// <summary>
  /// Specifies an array data in Omron PLCs
  /// </summary>
  public class OmronArray : OmronData
  {
    #region ATTRIBUTES

    /// <summary>
    /// Specifies the Omron Data type of every Item in the Array 
    /// </summary>
    public OmronData EmbeddedData { get; set; }

    /// <summary>
    /// Specifies how many dimensions the array has
    /// </summary>
    public ushort Dimensions { get; set; }

    /// <summary>
    /// Indicates the number of items contained in the array
    /// </summary>
    public uint Length { get; set; }

    #endregion

    #region CONSTRUCTORS

    public OmronArray(OmronData embedded, ushort dimensions, uint length) : base(embedded.Size)
    {
      EmbeddedData = embedded;
      Dimensions = dimensions;
      Length = length;
    }

    #endregion
  }
}
