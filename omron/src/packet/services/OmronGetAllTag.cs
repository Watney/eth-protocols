﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         13-02-2020
 *  
 *  TITLE:        Omron Tag Get All Tags object
 *  DESCRIPTION:  Omron specific service used to retreive all public tags from PLC
 */

using Cip;
using UtilByte;
using System;
using System.IO;

namespace Omron
{
  /// <summary>
  /// Omron specific service used to retreive all public tags from PLC
  /// </summary>
  public class OmronGetAllTag : CipService
  {
    static CipSegmentPath TagNameServer = new CipSegmentPath(
      new CipLogicalSegment(0x6A, CipLogicalType.Class),
      new CipLogicalSegment(0x00, CipLogicalType.Instance));

    #region ATTRIBUTES

    /// <summary>
    /// Number of PLC public tags requested
    /// </summary>
    public uint TagsCount { get; set; }

    /// <summary>
    /// PLC tags responsed
    /// </summary>
    public OmronTag[] Tags { get; set; }

    #endregion

    #region CONSTRUCTORS

    public OmronGetAllTag(uint count) : base((byte)OmronServiceID.GetAllTags, TagNameServer)
    {
      TagsCount = count;
      Tags = null;
    }

    public OmronGetAllTag(params OmronTag[] tags) : base((byte)OmronServiceID.GetAllTags, null, false)
    {
      Tags = tags;
      TagsCount = (uint)tags.Length;
    }

    public OmronGetAllTag(MemoryStream stream, int length = 0) : base(stream, length)
    {

    }

    #endregion

    #region IMPLEMENT INTERFACE

    protected override byte[] SerializeRequest()
    {
      #region SERIALIZE DATA

      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(new byte[] { 0x01, 0x00, 0x00, 0x00 });
      writer.Write(TagsCount);
      writer.Write(new byte[] { 0x02, 0x00 });

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] data = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), data, data.Length);

      #endregion

      #endregion

      CipMessageRouterRequest request = new CipMessageRouterRequest(Id, RequestPath, data);
      return request.Serialize();
    }

    protected override byte[] SerializeResponse()
    {
      #region SERIALIZE DATA

      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write((uint)Tags.Length);

      writer.Write(new byte[] { 0x01, 0x00, 0x00, 0x00 });
      writer.Write(new byte[] { 0x0C, 0x00 });

      foreach (OmronTag tag in Tags)
        writer.Write(tag.Serialize());

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      //  In this service the last tag ends only with its name (solast 6 bytes are removed)
      byte[] data = new byte[writer.Index - 6];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), data, data.Length);

      #endregion

      #endregion

      CipMessageRouterResponse response = new CipMessageRouterResponse(Id, data, CipStatus.SUCCESS);
      return response.Serialize();
    }

    protected override void DeserializeRequest(MemoryStream stream)
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(stream);
      RequestPath = request.Path;

      #region DESERIALIZE DATA

      ByteReader reader = new ByteReader(new MemoryStream(request.Data));

      reader.ReadBytes(4);
      TagsCount = reader.ReadUInt32();
      reader.ReadBytes(2);

      #endregion
    }

    protected override void DeserializeResponse(MemoryStream stream)
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(stream);

      if (response.Status != CipStatus.SUCCESS)
        throw new CipException(response.Status, response.AdditionalStatus);

      #region DESERIALIZE DATA

      ByteReader reader = new ByteReader(new MemoryStream(response.Data));

      Tags = new OmronTag[reader.ReadUInt32()];

      reader.ReadBytes(4);
      reader.ReadBytes(2);

      for (int i = 0; i < Tags.Length; i++)
        Tags[i] = new OmronTag(reader.BaseStream as MemoryStream, false);

      #endregion
    }

    #endregion
  }
}
