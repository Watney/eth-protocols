﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         12-02-2020
 *  
 *  TITLE:        Omron Service ID enum
 *  DESCRIPTION:  Specifies the Omron specific Service code
 */

namespace Omron
{
  /// <summary>
  /// Specifies the Omron specific Service code
  /// </summary>
  public enum OmronServiceID
  {
    GetAllTags = 0x5F
  }
}
