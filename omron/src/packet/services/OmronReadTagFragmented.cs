﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         19-02-2020
 *  
 *  TITLE:        Omron Read Tag Fragmented object
 *  DESCRIPTION:  Service used to read data that does not fit a single packet from a tag variable stored in the PLC
 */

using Cip;
using UtilByte;
using System;
using System.IO;
using System.Collections.Generic;

namespace Omron
{
  /// <summary>
  /// Service used to read data that does not fit a single packet from a tag variable stored in the PLC
  /// </summary>
  public class OmronReadTagFragmented : CipService
  {
    public const ushort MAX_SIZE = 490;

    #region ATTRIBUTES

    /// <summary>
    /// Tag name of the variable requested
    /// </summary>
    public string RequestTag { get; set; }

    /// <summary>
    /// Initial offset of bytes from which start to read
    /// </summary>
    public ushort RequestOffset { get; set; }

    /// <summary>
    /// Bytes count to be read
    /// </summary>
    public ushort RequestSize { get; set; }

    /// <summary>
    /// Requested items count (DEFAULT 1)
    /// </summary>
    public ushort RequestCount { get; set; }

    /// <summary>
    /// Responsed data value
    /// </summary>
    public byte[] ResponseData { get; set; }

    #endregion

    #region CONSTRUCTORS

    public OmronReadTagFragmented(string tag, ushort offset, ushort size, ushort count = 1) : 
      base((byte)CipServiceID.ReadTag, GetRequestPath(tag, offset, size))
    {
      RequestTag = tag;
      RequestOffset = offset;
      RequestSize = size;
      RequestCount = count;

      if (RequestSize > MAX_SIZE)
        throw new OverflowException(string.Format("Request Size cannot be > {0}", MAX_SIZE));
    }

    public OmronReadTagFragmented(byte[] response) : base ((byte)CipServiceID.ReadTag, null, false)
    {
      ResponseData = response;
    }

    public OmronReadTagFragmented(MemoryStream stream, int length = 0) : base (stream, length)
    {

    }

    #endregion

    private static CipSegmentPath GetRequestPath(string tag, ushort offset, ushort size)
    {
      List<CipSegment> segs = new List<CipSegment>();

      segs.AddRange(CipDataManager.GetRequestPath(tag).Segments);
      
      #region SERIALIZE DATA

      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(offset);
      writer.Write((ushort)0);
      writer.Write(size);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] data = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), data, data.Length);

      #endregion

      #endregion

      segs.Add(new CipDataSegment(data));

      return new CipSegmentPath(segs.ToArray());
    }

    #region IMPLEMENT INTERFACE

    protected override byte[] SerializeRequest()
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(Id, RequestPath, BitConverter.GetBytes(RequestCount));
      return request.Serialize();
    }

    protected override byte[] SerializeResponse()
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(Id, ResponseData, CipStatus.SUCCESS);
      return response.Serialize();
    }

    protected override void DeserializeRequest(MemoryStream stream)
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(stream);
      RequestPath = request.Path;

      RequestTag = CipDataManager.GetTag(RequestPath);
      RequestOffset = BitConverter.ToUInt16((RequestPath.Segments[RequestPath.Segments.Length - 3] as CipDataSegment).Data, 0);
      RequestSize = BitConverter.ToUInt16((RequestPath.Segments[RequestPath.Segments.Length - 1] as CipDataSegment).Data, 0);
      RequestCount = BitConverter.ToUInt16(request.Data, 0);
    }

    protected override void DeserializeResponse(MemoryStream stream)
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(stream);

      if (response.Status != CipStatus.SUCCESS)
        throw new CipException(response.Status, response.AdditionalStatus);

      ResponseData = response.Data;
    }

    #endregion
  }
}
