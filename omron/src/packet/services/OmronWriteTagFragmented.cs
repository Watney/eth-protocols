﻿
/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         12-02-2020
 *  
 *  TITLE:        Omron Write Tag Fragmented object
 *  DESCRIPTION:  Service used to write data that does not fit in a single packet in a tag variable stored in the PLC
 */

using Cip;
using System;
using System.IO;
using UtilByte;
using System.Collections.Generic;

namespace Omron
{
  /// <summary>
  /// Service used to write data that does not fit in a single packet in a tag variable stored in the PLC
  /// </summary>
  public class OmronWriteTagFragmented : CipService
  {
    public const ushort MAX_SIZE = 440;

    #region ATTRIBUTES

    /// <summary>
    /// Tag name of the variable requested
    /// </summary>
    public string RequestTag { get; set; }

    /// <summary>
    /// Initial offset of bytes from which start to write
    /// </summary>
    public ushort RequestOffset { get; set; }

    /// <summary>
    /// Bytes count to be write
    /// </summary>
    public ushort RequestSize { get; set; }

    /// <summary>
    /// Requested items count (DEFAULT 1)
    /// </summary>
    public ushort RequestCount { get; set; }

    /// <summary>
    /// Requested partial data value
    /// </summary>
    public byte[] RequestPartialData { get; set; }

    #endregion

    #region CONSTRUCTORS

    public OmronWriteTagFragmented(string tag, byte[] data, ushort offset, ushort size, ushort count = 1) :
      base((byte)CipServiceID.WriteTag, GetRequestPath(tag, offset, size))
    {
      RequestTag = tag;
      RequestOffset = offset;
      RequestSize = size;
      RequestPartialData = data;

      if (RequestSize > MAX_SIZE)
        throw new OverflowException(string.Format("Request Size cannot be > {0}", MAX_SIZE));
    }

    public OmronWriteTagFragmented() : base((byte)CipServiceID.WriteTag, null, false)
    {
    }

    public OmronWriteTagFragmented(MemoryStream stream, int length = 0) : base(stream, length)
    {

    }

    #endregion

    private static CipSegmentPath GetRequestPath(string tag, ushort offset, ushort size)
    {
      List<CipSegment> segs = new List<CipSegment>();

      segs.AddRange(CipDataManager.GetRequestPath(tag).Segments);

      #region SERIALIZE DATA

      ByteWriter writer = new ByteWriter(new MemoryStream());

      writer.Write(offset);
      writer.Write((ushort)0);
      writer.Write(size);

      #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

      byte[] data = new byte[writer.Index];
      Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), data, data.Length);

      #endregion

      #endregion

      segs.Add(new CipDataSegment(data));

      return new CipSegmentPath(segs.ToArray());
    }

    #region IMPLEMENT INTERFACE

    protected override byte[] SerializeRequest()
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(Id, RequestPath, RequestPartialData);
      return request.Serialize();
    }

    protected override byte[] SerializeResponse()
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(Id, new byte[0], CipStatus.SUCCESS);
      return response.Serialize();
    }

    protected override void DeserializeRequest(MemoryStream stream)
    {
      CipMessageRouterRequest request = new CipMessageRouterRequest(stream);
      RequestPath = request.Path;

      RequestTag = CipDataManager.GetTag(RequestPath);
      RequestPartialData = request.Data;
    }

    protected override void DeserializeResponse(MemoryStream stream)
    {
      CipMessageRouterResponse response = new CipMessageRouterResponse(stream);

      if (response.Status != CipStatus.SUCCESS)
        throw new CipException(response.Status, response.AdditionalStatus);
    }

    #endregion
  }
}
