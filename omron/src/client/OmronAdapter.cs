﻿/*
 *  AUTHOR:       Ghiotto Mirko 
 *  COMPANY:      Color Service SRL
 *  DATE:         17-03-2020
 *  
 *  TITLE:        Omron Adapter object
 *  DESCRIPTION:  Synchronous Omron Adapter
 */

using Cip;
using UtilByte;
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace Omron
{
  /// <summary>
  /// Synchronous Omron Adapter
  /// </summary>
  public class OmronAdapter : CipUnconnectedAdapter
  {
    #region CONSTRUCTORS

    public OmronAdapter(string ip) : base(ip)
    {

    }

    public OmronAdapter(string ip, ushort port) : base(ip, port)
    {

    }

    #endregion

    #region SPECIFIC OBJECTS FUNCTIONS

    /// <summary>
    /// Returns the instance specified of the Omron Tag specific object
    /// </summary>
    public OmronTag GetTag(ushort instance)
    {
      return new OmronTag(new MemoryStream(GetInstance(0x6A, instance)));
    }

    /// <summary>
    /// Returns the instance specified of the Omron Variable specific object
    /// </summary>
    public OmronVariable GetVariable(ushort instance)
    {
      return new OmronVariable(new MemoryStream(GetInstance(0x6B, instance)));
    }

    /// <summary>
    /// Returns the instance specified of the Omron Variable specific object
    /// </summary>
    public OmronVariable GetVariable(string tag)
    {
      CipGetAllAttribute getInst = new CipGetAllAttribute(tag);
      byte[] resp = new CipGetAllAttribute(new MemoryStream(SendUnconnected(getInst))).ResponseData;

      return new OmronVariable(new MemoryStream(resp));
    }

    /// <summary>
    /// Returns the instance specified of the Omron Variable Type specific object
    /// </summary>
    public OmronVariableType GetVariableType(ushort instance)
    {
      return new OmronVariableType(new MemoryStream(GetInstance(0x6C, instance)));
    }


    private byte[] GetInstance(ushort classId, ushort instanceId)
    {
      CipGetAllAttribute getInst = new CipGetAllAttribute(classId, instanceId);
      return new CipGetAllAttribute(new MemoryStream(SendUnconnected(getInst))).ResponseData;
    }

    #endregion

    #region TAG FUNCTION

    /// <summary>
    /// Returns all public tags stored in the PLC with their data structure
    /// </summary>
    public Dictionary<string, OmronData> GetPublicTags()
    {
      #region   GET PUBLIC TAGS COUNT

      CipGetSingleAttribute getCount = new CipGetSingleAttribute(0x6A, 0x00, 0x68);
      byte[] response = SendUnconnected(getCount);

      getCount = new CipGetSingleAttribute(new MemoryStream(response));
      uint count = BitConverter.ToUInt32(getCount.ResponseData, 0);

      #endregion

      #region GET PUBLIC TAGS

      List<OmronTag> tags = new List<OmronTag>();

      OmronGetAllTag getTags = new OmronGetAllTag(count);
      response = SendUnconnected(getTags);

      getTags = new OmronGetAllTag(new MemoryStream(response));
      tags.AddRange(getTags.Tags);

      #endregion

      #region GET TAGS VARIABLES

      List<OmronVariable> variables = new List<OmronVariable>();

      foreach (OmronTag tag in tags)
        variables.Add(GetVariable(tag.InstanceID));

      #endregion

      #region GET TAGS VARIABLE TYPES

      Dictionary<ushort, OmronData> structs = new Dictionary<ushort, OmronData>();

      foreach (OmronVariable variable in variables.Distinct().Where(v => v.IsStruct))
        structs.Add(variable.InstanceID, GetData(GetVariableType(variable.InstanceID)));

      #endregion

      Dictionary<string, OmronData> result = new Dictionary<string, OmronData>();

      for (int i = 0; i < tags.Count; i++)
      {
        OmronData data = null;

        //  Tag is ATOMIC
        if (!variables[i].IsArray && !variables[i].IsStruct)
          data = new OmronAtomic(variables[i].TagType, (int)variables[i].Size);

        //  Tag is ARRAY of ATOMIC
        else if (variables[i].IsArray && !variables[i].IsStruct)
          data = new OmronArray(new OmronAtomic(variables[i].TagType, (int)variables[i].Size), variables[i].Dimensions, variables[i].Length);

        //  Tag is STRUCT
        else if (!variables[i].IsArray && variables[i].IsStruct)
          data = structs[variables[i].InstanceID];

        //  Tag is ARRAY of STRUCT
        else
          data = new OmronArray(structs[variables[i].InstanceID], variables[i].Dimensions, variables[i].Length);

        result.Add(tags[i].SymbolicName, data);
      }

      return result;
    }

    /// <summary>
    /// Recursive function used to retreive an Omron Data object from one or more Omron Variable Types stored in PLC
    /// </summary>
    private OmronData GetData(OmronVariableType type)
    {
      OmronData data = null;

      if (type.IsField)
      {
        if (type.IsStruct)
          data = type.IsArray
            ? new OmronArray(GetData(GetVariableType(type.InstanceID)), type.Dimensions, type.Length)
            : GetData(GetVariableType(type.InstanceID));

        else
          data = type.IsArray
            ? new OmronArray(new OmronAtomic(type.SecondaryTagType, (int)type.Size), type.Dimensions, type.Length)
            : (OmronData)new OmronAtomic(type.TagType, (int)type.Size);
      }

      else
      {
        if (type.IsStruct)
        {
          OmronStruct str = new OmronStruct(type.SymbolicName, type.StructID, type.FieldsCount, (int)type.Size);
          data = type.IsArray ? new OmronArray(str, type.Dimensions, type.Length) : (OmronData)str;

          for (int i = 0; i < type.FieldsCount; i++)
          {
            OmronVariableType ovt = GetVariableType((ushort)(type.InstanceID + i));

            if (type.IsArray)
              ((data as OmronArray).EmbeddedData as OmronStruct).Fields.Add(ovt.SymbolicName, GetData(ovt));
            else
              (data as OmronStruct).Fields.Add(ovt.SymbolicName, GetData(ovt));
          }
        }
      }

      return data;
    }

    #endregion

    #region FUNCTIONS TO READ & WRITE TAGS

    /// <summary>
    /// Function used to retreive a PLC tag of ARRAY type
    /// </summary>
    public new T ReadTag<T>(string variable, int length, ushort count = 1)
    {
      T response;
      bool isStruct = false;
      int totalSize = 0;

      //  Calculate total size of the tag to be read
      try
      {
        totalSize = CipDataManager.Sizes[typeof(T)] * length;
        isStruct = false;
      }
      catch (KeyNotFoundException)
      {
        CipStruct str = CipDataManager.GetStruct(typeof(T));

        if (str is null)
          throw new InvalidCastException("Cannot deserialize a struct without the \"CipStruct\" attribute");

        totalSize = str.Size * length;
        isStruct = true;
      }

      // Check if it can be read in one single packet
      if (totalSize <= MAX_RESPONSE_SIZE)
        response = base.ReadTag<T>(variable, count);

      //  Otherwise it will be read fragmented in several packets
      else
      {
        #region READ PLC TAG FRAGMENTED

        /*
         *  READ TAG FRAGMENT : READ FROM THE BEGINNING OF THE SERIALIZED TAG TO ITS END
         */

        ByteWriter writer = new ByteWriter(new MemoryStream());
        int initialOffset = 0;

        while (totalSize > initialOffset)
        {
          //  Calculate how much byte read 
          int size = ((totalSize - initialOffset) > MAX_RESPONSE_SIZE)
            ? (int)MAX_RESPONSE_SIZE
            : (totalSize - initialOffset);

          //  Get partial data from PLC
          OmronReadTagFragmented service = new OmronReadTagFragmented(variable, (ushort)initialOffset, (ushort)size);
          service = new OmronReadTagFragmented(new MemoryStream(SendUnconnected(service)));

          //  Only from first packet read type tag
          if (initialOffset > 0)
          {
            int idx = isStruct ? 4 : 2;
            writer.Write(service.ResponseData, idx, size);
          }
          else
            writer.Write(service.ResponseData);

          //  Increment reading offset
          initialOffset += size;
        }

        #endregion

        writer.Index = 0;
        response = CipDataManager.Deserialize<T>(writer.BaseStream as MemoryStream, length, count, false);
      }

      return response;
    }

    /// <summary>
    /// Function used to retreive a PLC tag of ATOMIC or STRUCT type
    /// </summary>
    public new T ReadTag<T>(string variable, ushort count = 1)
    {
      return ReadTag<T>(variable, 1, count);
    }

    /// <summary>
    /// Function used to write data in a PLC tag
    /// </summary>
    public new void WriteTag<T>(string variable, T value)
    {
      bool isStruct = false;
      int totalSize = 0;

      int length = 1;
      Type type = typeof(T).IsArray ? (value as Array).GetValue(0).GetType() : typeof(T);

      //  If data is array calculate its size like [ ITEM TYPE * ARRAY LENGTH ]
      if (typeof(T).IsArray)
        length = (value as Array).Length;

      //  Calculate total size of the data to be write
      try
      {
        totalSize = CipDataManager.Sizes[type] * length;
        isStruct = false;
      }
      catch (KeyNotFoundException)
      {
        CipStruct str = CipDataManager.GetStruct(type);

        if (str is null)
          throw new InvalidCastException("Cannot deserialize a struct without the \"CipStruct\" attribute");

        totalSize = str.Size * length;
        isStruct = true;
      }

      // Check if it can be write in one single packet
      if (totalSize <= MAX_REQUEST_SIZE)
        base.WriteTag(variable, value);

      //  Otherwise it will be write fragmented in several packets
      else
      {
        #region WRITE PLC TAG FRAGMENTED

        /*
         *  WRITE TAG FRAGMENT : WRITE FROM THE END OF THE SERIALIZED DATA TO ITS BEGINNING
         */

        byte[] reqData = CipDataManager.Serialize(value, 1, true);
        int headerSize = isStruct ? 6 : 4;

        int initialOffset = (reqData.Length - headerSize) - (int)MAX_REQUEST_SIZE;
        int dataSize = 0;

        while (initialOffset >= 0)
        {
          //  Calculate how much byte write 
          dataSize = (initialOffset > MAX_REQUEST_SIZE) ? (int)MAX_REQUEST_SIZE : initialOffset;

          #region SERIALIZE REQUEST DATA

          ByteWriter writer = new ByteWriter(new MemoryStream());

          //  Write tag type
          writer.Write(reqData, 0, headerSize);
          //  Write partial data 
          writer.Write(reqData, (initialOffset + headerSize), dataSize);

          #region COPY THE VALUE OF THE STREAM IN A BYTE ARRAY

          byte[] data = new byte[dataSize + headerSize];
          Array.Copy((writer.BaseStream as MemoryStream).GetBuffer(), data, data.Length);

          #endregion

          #endregion

          //  Write partial data to PLC
          OmronWriteTagFragmented service = new OmronWriteTagFragmented(variable, data, (ushort)initialOffset, (ushort)dataSize, 1);
          base.SendUnconnected(service);

          //  Decrement writing offset
          initialOffset -= dataSize;
        }

        #endregion
      }
    }

    #endregion
  }
}
